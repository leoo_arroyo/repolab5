# Readme

# Tecnologias

JAVA 8

## pom.xml desc

Spring MVC [5.2.6]
Spring ORM [5.2.6]
Hibernate Core [5.4.7]
Hibernate Validator [5.4.1]
Servlet [4.0.1]
JSTL [1.2]
TagLibs [1.2.5]
MySQL [8.0.20]
Jackson Converter [2.11.0]
JUnit [3.8.1]

## Front

Bootstrap [4.5.0]
Fontawesome [5.10.0]
jQuery [3.4.1]
Popper.js [1.16.0]
DataTable JS [1.10.21]
SweetAlert [2]