package com.proyec.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.entities.Usuario;
import com.proyec.services.UsuarioService;
import com.proyec.services.generic.GenericService;
import com.proyec.utilweb.Response;

@Service
public class UsuarioServiceImpl extends GenericService<Usuario, String> implements UsuarioService {
	
	@Override
	@Transactional
	public Response<Usuario> Guardar(Usuario entity) {
		Response<Usuario> response = super.Guardar(entity);
		response.getData().setPassword("");
		return response;
	}
	
	@Override
	@Transactional
	public Response<Usuario> Login(String user, String password) {
		Response<Usuario> response = new Response<>();
		List<Usuario> result = dao.ExecuteNamedQuery("Usuario.Login", Map.of("user", user, "password", password));
		if (result.size() == 0) {
			response.setMessage("ERROR");
			response.getDetails().add("Usuario o contraseña incorrectos.");
		}
		else if (!result.get(0).getEstado()) {
			response.setMessage("ERROR");
			response.getDetails().add("Usuario deshabilitado.");
		}
		else {
			response.setMessage("OK");
			response.setData(result.get(0));
		}
		return response;
	}

}
