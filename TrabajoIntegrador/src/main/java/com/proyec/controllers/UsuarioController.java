package com.proyec.controllers;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyec.entities.Usuario;
import com.proyec.services.UsuarioService;
import com.proyec.services.UtilService;
import com.proyec.utilweb.CookiesHelper;
import com.proyec.utilweb.Response;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {
	
	@Autowired
	UsuarioService srvUsuario;
	
	@Autowired
	UtilService srvUtil;
	
	@GetMapping
	String getUsuarios(Model model, HttpServletResponse response) {
		CookiesHelper.SetCookie("lastPage", "/usuarios", response);
		
		model.addAttribute("listaUsuarios", srvUsuario.ObtenerTodos().getData());
		model.addAttribute("usuario", new Usuario());
		model.addAttribute("listaProvincias", srvUtil.ObtenerProvincias().getData());
		
		return "AdminUsuarios";
	}
	
	@GetMapping("/{user}")
	@ResponseBody
	Response<Usuario> getUser(@PathVariable String user) {
		return srvUsuario.Buscar(user);
	}
	
	@PostMapping
	@ResponseBody
	Response<Usuario> SaveOrUpdate(@ModelAttribute Usuario usuario, BindingResult result) {
		if (!result.hasErrors()) {
			return srvUsuario.Guardar(usuario);
		}
		else {
			System.out.println("error");
		}
		
		return new Response<>("ERROR", null);
	}

}
