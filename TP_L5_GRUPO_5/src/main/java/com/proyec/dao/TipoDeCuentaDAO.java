package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.TipoDeCuenta;

@Repository
public class TipoDeCuentaDAO extends GenericHibernateDAO<TipoDeCuenta> {

}
