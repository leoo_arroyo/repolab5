package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.EstadoDePrestamo;

@Repository
public class EstadoDePrestamoDAO extends GenericHibernateDAO<EstadoDePrestamo> {

}
