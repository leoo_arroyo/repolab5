package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.EstadoDeTransferencia;

@Repository
public class EstadoDeTransferenciaDAO extends GenericHibernateDAO<EstadoDeTransferencia> {

}
