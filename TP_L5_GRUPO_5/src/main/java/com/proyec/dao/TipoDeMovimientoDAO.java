package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.TipoDeMovimiento;

@Repository
public class TipoDeMovimientoDAO extends GenericHibernateDAO<TipoDeMovimiento> {

}
