package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.Usuario;

@Repository
public class UsuarioDAO extends GenericHibernateDAO<Usuario> {

}
