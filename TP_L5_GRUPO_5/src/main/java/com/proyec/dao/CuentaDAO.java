package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.Cuenta;

@Repository
public class CuentaDAO extends GenericHibernateDAO<Cuenta> {

}
