package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.Movimiento;

@Repository
public class MovimientoDAO extends GenericHibernateDAO<Movimiento> {

}
