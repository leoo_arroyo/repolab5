package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.Provincia;

@Repository
public class ProvinciaDAO extends GenericHibernateDAO<Provincia> {

}
