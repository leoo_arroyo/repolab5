package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.Transferencia;

@Repository
public class TransferenciaDAO extends GenericHibernateDAO<Transferencia> {

}
