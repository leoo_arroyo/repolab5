package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.Pais;

@Repository
public class PaisDAO extends GenericHibernateDAO<Pais> {

}
