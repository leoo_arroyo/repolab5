package com.proyec.dao;

import org.springframework.stereotype.Repository;

import com.proyec.dao.generic.GenericHibernateDAO;
import com.proyec.entities.Localidad;

@Repository
public class LocalidadDAO extends GenericHibernateDAO<Localidad> {

}
