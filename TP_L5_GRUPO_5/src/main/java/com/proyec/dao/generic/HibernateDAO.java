package com.proyec.dao.generic;

import java.util.List;
import java.util.Map;

public interface HibernateDAO<T> extends DAO<T> {

	Long Count();

	List<T> FindAll(int limit, int offset);

	List<T> ExecuteQuery(String hql);

	List<T> ExecuteQuery(String hql, int limit, int offset);

	List<T> ExecuteNamedQuery(String query, Map<String, Object> parameters);

	List<T> ExecuteNamedQuery(String query, Map<String, Object> parameters, int limit, int offset);

	List<T> ExecuteNamedStoredProcedureQuery(String nameSP, Map<String, Object> parameters);

}
