package com.proyec.dao.generic;

import java.util.List;
import java.util.Optional;

public interface DAO<T> {
	
	void Save(T entity);
	
	void Update(T entity);
	
	Optional<T> Find(Object key);
	
	List<T> FindAll();

}