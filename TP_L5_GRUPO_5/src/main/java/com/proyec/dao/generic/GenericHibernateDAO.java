package com.proyec.dao.generic;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;

import javax.persistence.StoredProcedureQuery;
import javax.persistence.TypedQuery;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class GenericHibernateDAO<T> implements HibernateDAO<T> {

	@Autowired
	protected SessionFactory sessionFactory;

	protected Class<T> entityClass;

	protected Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	protected GenericHibernateDAO() {
		Type type = getClass().getGenericSuperclass();
		ParameterizedType pt = (ParameterizedType) type;
		entityClass = (Class) pt.getActualTypeArguments()[0];
	}

	@Override
	public void Save(T entity) {
		Session s = getCurrentSession();
		s.clear();
		s.save(entity);
		s.flush();
		s.detach(entity);
	}

	@Override
	public void Update(T entity) {
		Session s = getCurrentSession();
		s.clear();
		s.update(entity);
		s.flush();
		s.detach(entity);
	}

	@Override
	public Optional<T> Find(Object key) {
		T entity = getCurrentSession().find(entityClass, key);
		if (entity != null)
			getCurrentSession().detach(entity);
		return Optional.ofNullable(entity);
	}

	@Override
	public List<T> FindAll() {
		return getCurrentSession().createQuery("FROM " + entityClass.getName(), entityClass).getResultList();
	}

	@Override
	public Long Count() {
		Long size = (Long) getCurrentSession().createQuery("SELECT COUNT(a) FROM " + entityClass.getName() + " a")
				.getSingleResult();
		return size;
	}

	@Override
	public List<T> FindAll(int limit, int offset) {
		Query<T> query = getCurrentSession().createQuery("FROM " + entityClass.getName(), entityClass);
		return query.setMaxResults(limit).setFirstResult(offset).getResultList();
	}

	@Override
	public List<T> ExecuteQuery(String hql) {
		return getCurrentSession().createQuery(hql, entityClass).getResultList();
	}

	@Override
	public List<T> ExecuteQuery(String hql, int limit, int offset) {
		Query<T> query = getCurrentSession().createQuery(hql, entityClass);
		query.setMaxResults(limit).setFirstResult(offset);
		return query.getResultList();
	}

	@Override
	public List<T> ExecuteNamedQuery(String query, Map<String, Object> parameters) {
		Session session = getCurrentSession();
		TypedQuery<T> namedQuery = session.createNamedQuery(query, entityClass);
		if (parameters != null) {
			for (Entry<String, Object> param : parameters.entrySet())
				namedQuery.setParameter(param.getKey(), param.getValue());
		}
		List<T> result = namedQuery.getResultList();
		session.clear();
		return result;
	}

	@Override
	public List<T> ExecuteNamedQuery(String query, Map<String, Object> parameters, int limit, int offset) {
		Session session = getCurrentSession();
		TypedQuery<T> namedQuery = session.createNamedQuery(query, entityClass);
		if (parameters != null) {
			for (Entry<String, Object> param : parameters.entrySet())
				namedQuery.setParameter(param.getKey(), param.getValue());
		}
		namedQuery.setMaxResults(limit).setFirstResult(offset);
		return namedQuery.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<T> ExecuteNamedStoredProcedureQuery(String nameSP, Map<String, Object> parameters) {
		Session session = getCurrentSession();
		StoredProcedureQuery spQuery = session.createNamedStoredProcedureQuery(nameSP);
		if (parameters != null) {
			for (Entry<String, Object> param : parameters.entrySet())
				spQuery.setParameter(param.getKey(), param.getValue());
		}
		return spQuery.getResultList();
	}

}