package com.proyec.controllers;

import java.util.Map;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyec.entities.Usuario;
import com.proyec.services.UsuarioService;
import com.proyec.services.UtilService;
import com.proyec.utilweb.CookiesHelper;
import com.proyec.utilweb.Response;

@Controller
@RequestMapping("/usuarios")
public class UsuarioController {

	@Autowired
	UsuarioService srvUsuario;

	@Autowired
	UtilService srvUtil;

	@Autowired
	ApplicationContext appContext;

	@GetMapping
	String GetUsuarios(Model model, HttpServletResponse response) {
		CookiesHelper.SetCookie("lastPage", "/usuarios", response);

		// Atributo para listar los usuarios
		model.addAttribute("listaUsuarios", srvUsuario.ObtenerTodos().getData());
		// Atributo para linkear al formulario
		model.addAttribute("usuario", appContext.getBean("usuario", Usuario.class));
		// Atributo para el DropDown de nacionalidades
		model.addAttribute("listaPaises", srvUtil.ObtenerPaises().getData());
		// Atributo para el DropDown de provincias
		model.addAttribute("listaProvincias", srvUtil.ObtenerProvincias().getData());

		return "AdminUsuarios";
	}

	@GetMapping("/{user}")
	@ResponseBody
	Response<Usuario> GetUsuario(@PathVariable String user) {
		var response = srvUsuario.BuscarPorNick(user);
		if (response.getMessage().equals("OK"))
			response.getData().setPassword("");
		
		return response;
	}

	@PostMapping("/save")
	@ResponseBody
	Response<Usuario> SaveUsuario(@ModelAttribute Usuario usuario, BindingResult result) {
		if (!result.hasErrors()) {
			return srvUsuario.Guardar(usuario);
		} else {
			System.out.println("error");
		}

		return new Response<>("ERROR", null);
	}

	@PostMapping("/update")
	@ResponseBody
	Response<Usuario> UpdateUsuario(@ModelAttribute Usuario usuario, BindingResult result) {
		if (!result.hasErrors()) {
			return srvUsuario.Actualizar(usuario);
		} else {
			System.out.println("error");
		}

		return new Response<>("ERROR", null);
	}

	@GetMapping("/delete/{user}")
	@ResponseBody
	Response<Usuario> DeleteUsuario(@PathVariable String user) {
		var response = srvUsuario.BuscarPorNick(user);
		if (response.getMessage().equals("OK")) {
			Usuario usuario = response.getData();
			usuario.setEstado(false);
			response = srvUsuario.Actualizar(usuario);
		}
		return response;
	}

	@GetMapping("/filter")
	@ResponseBody
	Response<Map<String, String>> getUsuariosFiltrados(@RequestParam("search") String search,
			@RequestParam("type") String type) {
		Map<String, String> users;

		switch (type) {
		case "byNombre":
			users = srvUsuario.ObtenerFiltradosPorUsuario(search).getData().stream()
					.collect(Collectors.toMap(Usuario::getDNI, Usuario::getUser));
			break;
			
		case "byDNI":
			users = srvUsuario.ObtenerFiltradosPorDNI(search).getData().stream()
					.collect(Collectors.toMap(Usuario::getDNI, Usuario::getDNI));
			break;

		default:
			users = null;
			break;
		}
		return (users.size() > 0) ? new Response<>("OK", users) : new Response<>("SIN RESULTADOS", null);
	}

}
