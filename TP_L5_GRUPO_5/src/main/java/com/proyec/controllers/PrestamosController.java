package com.proyec.controllers;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.proyec.entities.Cuota;
import com.proyec.entities.EstadoDePrestamo;
import com.proyec.entities.Movimiento;
import com.proyec.entities.Prestamo;
import com.proyec.entities.TipoDeMovimiento;
import com.proyec.services.CuentaService;
import com.proyec.services.CuotaService;
import com.proyec.services.MovimientoService;
import com.proyec.services.PrestamoService;
import com.proyec.services.UsuarioService;
import com.proyec.utilweb.CookiesHelper;
import com.proyec.utilweb.Response;

@Controller
@RequestMapping("/prestamos")
public class PrestamosController {

	@Autowired
	UsuarioService srvUsuarios;

	@Autowired
	PrestamoService srvPrestamo;

	@Autowired
	MovimientoService srvMovimientos;

	@Autowired
	CuentaService srvCuentas;

	@Autowired
	CuotaService srvCuotas;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpServletResponse response;

	@GetMapping
	String GetPrestamos(Model model, @CookieValue(value = "usuario", defaultValue = "") String user) {

		if (user.isEmpty())
			return "Login";

		var url = request.getServletPath();
		var slashIdx = url.lastIndexOf("/");
		if (slashIdx != 0)
			url = url.substring(0, slashIdx);

		CookiesHelper.SetCookie("lastPage", url, response);

		var usuario = srvUsuarios.BuscarPorNick(user).getData();

		var view = "";
		if (usuario.getPerfil() == 'A') {
			view = "AdminPrestamos";

			model.addAttribute("listaPrestamos", srvPrestamo.ObtenerTodos().getData());
		} else {
			view = "Prestamos";

			model.addAttribute("listaPrestamos", srvPrestamo.ObtenerPorUsuario(user).getData());
			model.addAttribute("prestamo", new Prestamo());
			model.addAttribute("listaCuentas", srvCuentas.ObtenerCuentasActivas(user).getData());
		}

		return view;
	}

	@GetMapping("/aceptar/{prestamoId}")
	@ResponseBody
	String AceptarPrestamo(@PathVariable Integer prestamoId) {
		var response = srvPrestamo.Buscar(prestamoId);
		if (response.getMessage().equals("OK")) {
			var prestamo = response.getData();

			prestamo.setEstado(new EstadoDePrestamo(2, null));
			prestamo.setFechaAprobacion(LocalDate.now());

			for (int i = 1; i <= prestamo.getPlazo(); i++) {
				Cuota c = new Cuota();
				c.setNumeroDeCuota(i);
				c.setEstadoDePago(false);
				c.setPrestamo(prestamo);

				srvCuotas.Guardar(c);
			}

			var c = srvCuentas.Buscar(prestamo.getCuenta().getCBU()).getData();
			c.setSaldo(c.getSaldo() + prestamo.getImporte());
			srvCuentas.Actualizar(c);

			var m = new Movimiento();
			m.setTipoMovimiento(new TipoDeMovimiento(2, null));
			m.setFecha(LocalDateTime.now());
			m.setImporte(prestamo.getImporte());
			m.setDetalle("Alta de préstamo");
			m.setCuenta(prestamo.getCuenta());
			m.setSigno('+');

			srvMovimientos.Guardar(m);

			response = srvPrestamo.Actualizar(prestamo);
		}
		return response.getMessage();
	}

	@GetMapping("/rechazar/{prestamoId}")
	@ResponseBody
	String RechazarPrestamo(@PathVariable Integer prestamoId) {
		var response = srvPrestamo.Buscar(prestamoId);
		if (response.getMessage().equals("OK")) {
			Prestamo prestamo = response.getData();
			prestamo.setEstado(new EstadoDePrestamo(3, null));

			response = srvPrestamo.Actualizar(prestamo);
		}
		return response.getMessage();
	}

	@GetMapping("/cuotas")
	ModelAndView ObtenerCuotas(@CookieValue(value = "usuario", defaultValue = "") String user,
			@RequestParam("idPrestamo") Integer idPrestamo) {

		if (user.isEmpty())
			return new ModelAndView("Login");

		var mv = new ModelAndView();

		var usuario = srvUsuarios.BuscarPorNick(user).getData();
		if (usuario.getPerfil() == 'A') {
			if (idPrestamo == null)
				return new ModelAndView("redirect:/prestamos");

			mv.setViewName("AdminCuotas");
		} else {
			if (idPrestamo == null)
				return new ModelAndView("redirect:/prestamos/usuarios");

			mv.setViewName("Cuotas");
			mv.addObject("listaCuentas", srvCuentas.ObtenerCuentas(user).getData());
		}

		mv.addObject("prestamo", srvPrestamo.Buscar(idPrestamo).getData());
		mv.addObject("listaCuotas", srvCuotas.ObtenerCuotas(idPrestamo).getData());
		return mv;
	}

	@PostMapping("/save")
	@ResponseBody
	Response<Prestamo> Save(@ModelAttribute Prestamo prestamo, BindingResult result,
			@CookieValue(value = "usuario", defaultValue = "") String user) {

		if (!result.hasErrors()) {
			prestamo.setUsuario(srvUsuarios.BuscarPorNick(user).getData());

			return srvPrestamo.Guardar(prestamo);
		} else {
			System.out.println("error");
		}

		return new Response<>("ERROR", null);
	}

	@PostMapping("/pagar")
	@ResponseBody
	Response<Cuota> Pagar(@RequestParam Map<String, String> params) {
		var idCuota = Integer.valueOf(params.get("idCuota"));
		var idCuenta = params.get("idCuenta");

		var cuenta = srvCuentas.Buscar(idCuenta).getData();
		var cuota = srvCuotas.Buscar(idCuota).getData();

		var saldo = cuenta.getSaldo() - cuota.getPrestamo().getImportePorCuota();
		if (saldo < 0)
			return new Response<Cuota>("ERROR").addDetail("La cuenta no dispone del saldo suficiente");
		else {
			cuenta.setSaldo(saldo);
			srvCuentas.Actualizar(cuenta);

			var m = new Movimiento();
			m.setCuenta(cuenta);
			m.setDetalle("Pago de préstamo");
			m.setImporte(cuota.getPrestamo().getImportePorCuota());
			m.setFecha(LocalDateTime.now());
			m.setSigno('-');
			m.setTipoMovimiento(new TipoDeMovimiento(3, null));

			srvMovimientos.Guardar(m);

			cuota.setEstadoDePago(true);
			cuota.setFechaDePago(LocalDate.now());
			return srvCuotas.Actualizar(cuota);
		}
	}
}
