package com.proyec.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.proyec.services.UsuarioService;
import com.proyec.services.UtilService;
import com.proyec.utilweb.CookiesHelper;

@Controller
public class MainController {

	@Autowired
	UtilService srvUtil;

	@Autowired
	UsuarioService srvUsuario;

	@GetMapping("/home")
	String home(Model model, @CookieValue(value = "usuario", defaultValue = "") String user,
			HttpServletRequest request) {
		
		if (user.isEmpty())
			return "Login";

		var response = srvUsuario.BuscarPorNick(user);
		if (!response.getMessage().equals("OK"))
			return "Login";

		model.addAttribute("user", user);

		var perfil = response.getData().getPerfil();
		
		var menus = srvUtil.ObtenerMenus(perfil).getData();
		model.addAttribute("listaMenus", menus);

		var lastPage = CookiesHelper.BuscarValorCookie("lastPage", request);
		if (lastPage.isEmpty())
			lastPage = (perfil == 'A') ? "/usuarios" : "/movimientos";
		model.addAttribute("lastPage", lastPage);

		return "MainPage";
	}

	@PostMapping("/login")
	String login(@RequestParam Map<String, String> params, Model model, HttpServletRequest request,
			HttpServletResponse response) {
		var srvResponse = srvUsuario.Login(params.get("txtEmail"), params.get("txtPassword"));
		if (srvResponse.getMessage().equals("OK")) {
			CookiesHelper.SetCookie("usuario", srvResponse.getData().getUser(), response);
			return home(model, srvResponse.getData().getUser(), request);
		} else {
			model.addAttribute("mensaje", srvResponse.getDetails().get(0));
			return "Login";
		}
	}

	@GetMapping("/logout")
	String logout(HttpServletResponse response) {
		CookiesHelper.DeleteCookie("usuario", response);
		CookiesHelper.DeleteCookie("lastPage", response);
		return "Login";
	}

}
