package com.proyec.controllers;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyec.services.CuentaService;
import com.proyec.services.PrestamoService;
import com.proyec.services.UtilService;
import com.proyec.utilweb.CookiesHelper;
import com.proyec.utilweb.Response;

@Controller
@RequestMapping("/reportes")
public class ReportesController {

	@Autowired
	PrestamoService srvPrestamo;

	@Autowired
	CuentaService srvCuenta;

	@Autowired
	UtilService srvUtil;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpServletResponse response;

	@GetMapping
	String getView(@CookieValue(value = "usuario", defaultValue = "") String user) {

		if (user.isEmpty())
			return "Login";

		var url = request.getServletPath();
		var slashIdx = url.lastIndexOf("/");
		if (slashIdx != 0)
			url = url.substring(0, slashIdx);

		CookiesHelper.SetCookie("lastPage", url, response);

		return "Reportes";
	}

	@GetMapping("/prestamos")
	@ResponseBody
	Response<List<Double>> ReportePrestamos() {
		var total = new ArrayList<>(Collections.nCopies(12, Double.valueOf(0.0)));

		for (var p : srvPrestamo.ObtenerTodos().getData()) {
			var mes = p.getFechaAprobacion().getMonthValue() - 1;
			var parcial = total.get(mes);
			total.set(mes, parcial + p.getImporte());
		}

		return new Response<>("OK", total);
	}

	@GetMapping("/cuentas")
	@ResponseBody
	Response<List<Integer>> ReporteCuentas(@RequestParam(name = "fechaInicio", defaultValue = "") String fechaInicio,
			@RequestParam(name = "fechaFin", defaultValue = "") String fechaFin) {

		LocalDate fInicio, fFin;

		fInicio = (fechaInicio.isEmpty()) ? LocalDate.now().minusMonths(1) : LocalDate.parse(fechaInicio);
		fFin = (fechaFin.isEmpty()) ? LocalDate.now() : LocalDate.parse(fechaFin);

		var listaCuentas = srvCuenta.ObtenerPorFechas(fInicio, fFin).getData();
		var totalXCuenta = new ArrayList<>(Collections.nCopies(5, Integer.valueOf(0)));

		for (var c : listaCuentas) {
			var idTipo = c.getTipoDeCuenta().getId() - 1;
			var parcial = totalXCuenta.get(idTipo);
			totalXCuenta.set(idTipo, ++parcial);
		}

		var total = 0;
		for (var n : totalXCuenta)
			total += n;

		final var tt = total;
		if (total != 0)
			totalXCuenta = (ArrayList<Integer>) totalXCuenta.stream().map(n -> (n * 100) / tt)
					.collect(Collectors.toList());

		return new Response<>("OK", totalXCuenta);
	}

}
