package com.proyec.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.proyec.entities.Cuenta;
import com.proyec.services.CuentaService;
import com.proyec.services.MovimientoService;
import com.proyec.services.UsuarioService;
import com.proyec.services.UtilService;
import com.proyec.utilweb.CookiesHelper;
import com.proyec.utilweb.Response;

@Controller
@RequestMapping("/movimientos")
public class MovimientosController {

	@Autowired
	UsuarioService srvUsuarios;

	@Autowired
	MovimientoService srvMovimientos;

	@Autowired
	CuentaService srvCuentas;

	@Autowired
	UtilService srvUtil;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpServletResponse response;

	@GetMapping
	String GetMovimientos(Model model, @CookieValue(value = "usuario", defaultValue = "") String user,
			@RequestParam(name = "alias", defaultValue = "", required = false) String alias) {

		if (user.isEmpty())
			return "Login";

		var url = request.getServletPath();
		var slashIdx = url.lastIndexOf("/");
		if (slashIdx != 0)
			url = url.substring(0, slashIdx);

		CookiesHelper.SetCookie("lastPage", url, response);

		var usuario = srvUsuarios.BuscarPorNick(user).getData();

		var view = "";
		if (usuario.getPerfil() == 'A') {
			view = "AdminMovimientos";

			model.addAttribute("listaTiposMov", srvUtil.ObtenerTiposDeMovimiento().getData());
			model.addAttribute("listaMovimientos", srvMovimientos.ObtenerTodos().getData());
		} else {
			view = "Movimientos";

			Cuenta cuentaActual = null;
			Response<Cuenta> rspCuenta = null;

			if (alias.isEmpty()) {
				rspCuenta = srvCuentas.ObtenerCuentaPrincipal(user);
				if (!rspCuenta.getMessage().equals("OK")) {
					model.addAttribute("message", rspCuenta.getMessage());
					return view;
				}
			} else
				rspCuenta = srvCuentas.Buscar(alias);

			cuentaActual = rspCuenta.getData();

			var result = srvMovimientos.ObtenerPorAlias(cuentaActual.getAlias()).getData();

			model.addAttribute("cuenta", cuentaActual);
			model.addAttribute("listaMovimientos", result);
		}

		return view;
	}

}
