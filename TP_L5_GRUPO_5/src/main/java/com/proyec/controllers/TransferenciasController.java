package com.proyec.controllers;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyec.entities.Cuenta;
import com.proyec.entities.Transferencia;
import com.proyec.services.CuentaService;
import com.proyec.services.MovimientoService;
import com.proyec.services.TransferenciaService;
import com.proyec.services.UsuarioService;
import com.proyec.services.UtilService;
import com.proyec.utilweb.CookiesHelper;
import com.proyec.utilweb.Response;

@Controller
@RequestMapping("/transferencias")
public class TransferenciasController {

	@Autowired
	UsuarioService srvUsuarios;

	@Autowired
	CuentaService srvCuentas;

	@Autowired
	TransferenciaService srvTransferencias;

	@Autowired
	MovimientoService srvMovimientos;

	@Autowired
	UtilService srvUtil;

	@GetMapping
	String GetTransferencias(Model model, @CookieValue(value = "usuario", defaultValue = "") String user,
			HttpServletRequest request, HttpServletResponse response) {

		if (user.isEmpty())
			return "Login";

		CookiesHelper.SetCookie("lastPage", request.getServletPath(), response);

		var usuario = srvUsuarios.BuscarPorNick(user).getData();

		var view = "";
		if (usuario.getPerfil() == 'A') {
			view = "AdminTransferencias";
			model.addAttribute("listaEstados", srvUtil.ObtenerEstadosDeTransferencia().getData());
			model.addAttribute("listaTransferencias", srvTransferencias.ObtenerTodos().getData());
		} else {
			view = "Transferencias";
			model.addAttribute("listaCuentas", srvCuentas.ObtenerCuentasActivas(user).getData());
			model.addAttribute("listaTransferencias", srvTransferencias.ObtenerPorUsuario(user).getData());
		}

		return view;
	}

	@PostMapping("/save")
	@ResponseBody
	Response<Transferencia> Save(@RequestParam Map<String, String> params) {
		try {
			var t = new Transferencia();
			t.setCuentaOrigen(new Cuenta(params.get("cmbCuentaOrigen")));
			t.setMonto(Double.valueOf(params.get("txtMonto").toString()));

			if (params.get("hdfTipoTrs").equals("propia"))
				t.setCuentaDestino(new Cuenta(params.get("cmbCuentaDestino")));
			else {
				if (params.get("opcBusqueda").equals("byCBU"))
					t.setCuentaDestino(new Cuenta(params.get("txtCBUDestino")));
				else
					t.setCuentaDestino(new Cuenta(params.get("txtAliasDestino")));
			}

			var response = srvTransferencias.Guardar(t);

			if (response.getMessage().equals("OK")) {
				var rspMovimientos = srvMovimientos.RegistrarPorTransferencia(response.getData());
				if (!rspMovimientos.getMessage().equals("OK")) {
					response.setMessage("ERROR");
					response.setDetails(rspMovimientos.getDetails());
				}
			}
			return response;
		} catch (NumberFormatException e) {
			var response = new Response<Transferencia>();
			response.setMessage("ERROR");
			response.getDetails().add("El monto debe ser un número");
			return response;
		} catch (Exception e) {
			e.printStackTrace();
			var response = new Response<Transferencia>();
			response.setMessage("ERROR");
			response.getDetails().add(e.getMessage());
			return response;
		}
	}

}
