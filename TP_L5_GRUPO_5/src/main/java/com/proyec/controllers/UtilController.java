package com.proyec.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyec.entities.Localidad;
import com.proyec.services.UtilService;

@Controller
@RequestMapping("/resources")
public class UtilController {
	
	@Autowired
	UtilService srvUtil;
	
	@GetMapping("/localidades")
	@ResponseBody
	List<Localidad> getLocalidades(@RequestParam("idProvincia") Integer idProvincia) {
		if (idProvincia == null)
			return List.of();
		else
			return srvUtil.ObtenerLocalidades(idProvincia).getData();
	}

}
