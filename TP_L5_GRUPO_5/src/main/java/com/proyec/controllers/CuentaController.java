package com.proyec.controllers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.CookieValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.proyec.entities.Cuenta;
import com.proyec.entities.Movimiento;
import com.proyec.entities.TipoDeMovimiento;
import com.proyec.services.CuentaService;
import com.proyec.services.MovimientoService;
import com.proyec.services.UsuarioService;
import com.proyec.services.UtilService;
import com.proyec.utilweb.CookiesHelper;
import com.proyec.utilweb.Response;

@Controller
@RequestMapping("/cuentas")
public class CuentaController {

	@Autowired
	UsuarioService srvUsuarios;

	@Autowired
	CuentaService srvCuentas;

	@Autowired
	MovimientoService srvMovimientos;

	@Autowired
	UtilService srvUtil;

	@Autowired
	HttpServletRequest request;

	@Autowired
	HttpServletResponse response;

	@GetMapping
	String GetCuentas(Model model, @CookieValue(value = "usuario", defaultValue = "") String user) {

		if (user.isEmpty())
			return "Login";

		var url = request.getServletPath();
		var slashIdx = url.lastIndexOf("/");
		if (slashIdx != 0)
			url = url.substring(0, slashIdx);

		CookiesHelper.SetCookie("lastPage", url, response);

		var usuario = srvUsuarios.BuscarPorNick(user).getData();

		var view = "";
		if (usuario.getPerfil() == 'A') {
			view = "AdminCuentas";
			model.addAttribute("listaCuentas", srvCuentas.ObtenerTodos().getData());
			model.addAttribute("cuenta", new Cuenta());
			model.addAttribute("listaTiposCuenta", srvUtil.ObtenerTiposDeCuenta().getData());
		} else {
			view = "CuentasUsuario";
			var result = srvCuentas.ObtenerCuentasActivas(user).getData();
			if (!result.isEmpty())
				model.addAttribute("listaCuentas", result);
			else
				model.addAttribute("message", "Sin cuentas");
		}

		return view;
	}

	@GetMapping("/{cbu}")
	@ResponseBody
	Response<Cuenta> GetCuenta(@PathVariable String cbu) {
		return srvCuentas.Buscar(cbu);
	}

	@PostMapping("/save")
	@ResponseBody
	Response<Cuenta> SaveCuenta(@ModelAttribute Cuenta Cuenta, BindingResult result) {
		if (!result.hasErrors()) {
			var response = srvCuentas.Guardar(Cuenta);
			if (response.getMessage().equals("OK")) {
				Movimiento m = new Movimiento();
				m.setTipoMovimiento(new TipoDeMovimiento(1, null));
				m.setImporte(10000.00);
				m.setSigno('+');
				m.setDetalle("Alta de cuenta");
				m.setCuenta(response.getData());

				srvMovimientos.Guardar(m);
			}
			return response;
		} else {
			return new Response<Cuenta>("ERROR").addDetail("Hay errores en los campos ingresados");
		}
	}

	@PostMapping("/update")
	@ResponseBody
	Response<Cuenta> UpdateCuenta(@ModelAttribute Cuenta Cuenta, BindingResult result) {
		if (!result.hasErrors())
			return srvCuentas.Actualizar(Cuenta);
		else
			return new Response<Cuenta>("ERROR").addDetail("Hay errores en los campos ingresados");
	}

	@GetMapping("/delete/{cbu}")
	@ResponseBody
	Response<Cuenta> DeleteCuenta(@PathVariable String cbu) {
		Cuenta cuenta = srvCuentas.Buscar(cbu).getData();
		cuenta.setEstado(false);
		return srvCuentas.Actualizar(cuenta);
	}

	@GetMapping("/check")
	@ResponseBody
	Response<Object> CheckCount(@RequestParam("user") String user) {
		if (srvCuentas.ObtenerCuentas(user).getData().size() == 4)
			return new Response<Object>("LIMIT", null);
		else
			return new Response<Object>("OK", null);
	}

	@GetMapping("/add")
	String AddCuenta(Model model, @RequestParam("user") String pUser,
			@CookieValue(value = "usuario", defaultValue = "") String user) {
		model.addAttribute("user", pUser);
		return GetCuentas(model, user);
	}

	@GetMapping("/edit")
	String EditFromUsuario(Model model, @RequestParam("alias") String alias,
			@CookieValue(value = "usuario", defaultValue = "") String user) {

		Cuenta cuenta = srvCuentas.Buscar(alias).getData();
		model.addAttribute("user", cuenta.getUsuario().getDNI());
		model.addAttribute("cuentaEdit", cuenta);
		return GetCuentas(model, user);
	}

	@GetMapping("/setprincipal")
	@ResponseBody
	Response<Cuenta> SetPrincipal(@RequestParam("alias") String alias,
			@CookieValue(value = "usuario", defaultValue = "") String user) {

		return srvCuentas.ActualizarCuentaPrincipal(user, alias);
	}
}
