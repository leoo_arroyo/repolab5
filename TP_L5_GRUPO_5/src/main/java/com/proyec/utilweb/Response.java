package com.proyec.utilweb;

import java.util.LinkedList;
import java.util.List;

public class Response<T> {

	private String message;

	private T data;

	private List<String> details = new LinkedList<>();

	public Response() {
		super();
	}
	
	public Response(String message) {
		super();
		this.message = message;
	}

	public Response(String message, T data) {
		super();
		this.message = message;
		this.data = data;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

	public List<String> getDetails() {
		return details;
	}

	public void setDetails(List<String> details) {
		this.details = details;
	}
	
	public Response<T> addDetail(String info) {
		this.details.add(info);
		return this;
	}

	@Override
	public String toString() {
		return "Response [message=" + message + ", data=" + data + ", details=" + details + "]";
	}
	
}