package com.proyec.utilweb;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.util.CookieGenerator;

public class CookiesHelper {
	
	public static String BuscarValorCookie(String nombreCookie, HttpServletRequest request) {
		String valor = "";
		Cookie[] cookies = request.getCookies();

		if (cookies != null) {
			for(Cookie c : cookies) {
				if (c.getName().equals(nombreCookie)) {
					valor = c.getValue();
					break;
				}
			}
		}
		return valor;
	}
	
	public static void SetCookie(String nombreCookie, String valor, HttpServletResponse response) {
		CookieGenerator cg = new CookieGenerator();
		cg.setCookieName(nombreCookie);
		cg.setCookiePath(CookieGenerator.DEFAULT_COOKIE_PATH);
		cg.setCookieMaxAge(60 * 10);
		cg.addCookie(response, valor);
	}
	
	public static void DeleteCookie(String nombreCookie, HttpServletResponse response) {
		CookieGenerator cg = new CookieGenerator();
		cg.setCookieName(nombreCookie);
		cg.setCookiePath(CookieGenerator.DEFAULT_COOKIE_PATH);
		cg.setCookieMaxAge(0);
		cg.addCookie(response, "");
	}

}