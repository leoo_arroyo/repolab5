package com.proyec.config;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.dao.generic.HibernateDAO;
import com.proyec.entities.EstadoDePrestamo;
import com.proyec.entities.EstadoDeTransferencia;
import com.proyec.entities.Localidad;
import com.proyec.entities.Pais;
import com.proyec.entities.Provincia;
import com.proyec.entities.TipoDeCuenta;
import com.proyec.entities.TipoDeMovimiento;
import com.proyec.entities.Usuario;
import com.proyec.entities.structure.Menu;
import com.proyec.entities.structure.Pagina;

@Component
public class InitApplication implements ApplicationListener<ContextRefreshedEvent> {
	
	@Autowired
	HibernateDAO<Menu> daoMenu;
	
	@Autowired
	HibernateDAO<Pais> daoPais;
	
	@Autowired
	HibernateDAO<Provincia> daoProvincia;
	
	@Autowired
	HibernateDAO<Localidad> daoLocalidad;
	
	@Autowired
	HibernateDAO<EstadoDePrestamo> daoEstPrestamo;
	
	@Autowired
	HibernateDAO<EstadoDeTransferencia> daoEstTransferencia;
	
	@Autowired
	HibernateDAO<TipoDeCuenta> daoTiposCuenta;
	
	@Autowired
	HibernateDAO<TipoDeMovimiento> daoTiposMovimiento;
	
	@Autowired
	HibernateDAO<Usuario> daoUsuario;

	@Override
	@Transactional
	public void onApplicationEvent(ContextRefreshedEvent event) {
		checkMenus();
		checkPaises();
		checkProvincias();
		checkLocalidades();
		checkEstadosPrestamos();
		checkEstadosTransferencias();
		checkTiposCuentas();
		checkTiposMovimientos();
		checkUserAdmin();
	}
	
	private <T> void persistirLista(HibernateDAO<T> dao, List<T> lista) {
		lista.forEach(e -> dao.Save(e));
	}
	
	private void checkMenus() {
		if (daoMenu.Count() == 0) {
			
			// Menus de Administrador
			Menu menu = new Menu();
			menu.setNombre("Administrar");
			menu.setPerfil('A');
			daoMenu.Save(menu);
			
			menu.addPagina(new Pagina("Usuarios", "/usuarios"));
			menu.addPagina(new Pagina("Cuentas", "/cuentas"));
			menu.addPagina(new Pagina("Prestamos", "/prestamos"));
			menu.addPagina(new Pagina("Movimientos", "/movimientos"));
			menu.addPagina(new Pagina("Transferencias", "/transferencias"));
			
			daoMenu.Update(menu);
			
			menu = new Menu();
			menu.setNombre("Reportes");
			menu.setPerfil('A');
			daoMenu.Save(menu);
			
			menu.addPagina(new Pagina("Informes", "/reportes"));
			
			daoMenu.Update(menu);
			
			// Menus de clientes
			menu = new Menu();
			menu.setNombre("Usuario");
			menu.setPerfil('C');
			daoMenu.Save(menu);
			
			menu.addPagina(new Pagina("Movimientos", "/movimientos"));
			menu.addPagina(new Pagina("Transferencias", "/transferencias"));
			menu.addPagina(new Pagina("Prestamos", "/prestamos"));
			menu.addPagina(new Pagina("Mis cuentas", "/cuentas"));
			
			daoMenu.Update(menu);
		}
	}
	
	private void checkPaises() {
		if (daoPais.Count() == 0) {
			List<Pais> paises = List.of(
					new Pais(null, "Argentina"),
					new Pais(null, "Bolivia"),
					new Pais(null, "Brasil"),
					new Pais(null, "Chile"),
					new Pais(null, "Colombia"),
					new Pais(null, "Ecuador"),
					new Pais(null, "Paraguay"),
					new Pais(null, "Perú"),
					new Pais(null, "Uruguay"),
					new Pais(null, "Venezuela")
			);
			persistirLista(daoPais, paises);
		}
	}
	
	private void checkProvincias() {
		if (daoProvincia.Count() == 0) {
			List<Provincia> provincias = List.of(
					new Provincia(2, "Buenos Aires", "AR-B"),
					new Provincia(3, "Catamarca", "AR-K"),
					new Provincia(6, "Entre Ríos", "AR-E"),
					new Provincia(8, "Mendoza", "AR-M"),
					new Provincia(9, "La Rioja", "AR-F"),
					new Provincia(10, "Salta", "AR-A"),
					new Provincia(15, "Tucumán", "AR-T"),
					new Provincia(16, "Chaco", "AR-H"),
					new Provincia(23, "Santa Cruz", "AR-Z"),
					new Provincia(24, "Tierra del Fuego", "AR-V")
			);
			persistirLista(daoProvincia, provincias);
		}
	}
	
	private void checkLocalidades() {
		if (daoLocalidad.Count() == 0) {
			
			List<Localidad> localidades =  List.of(
					// Buenos Aires
					new Localidad(21601,"RAFAEL CALZADA",2,1847),
					new Localidad(2584,"LOS LEONES",2,7000),
					new Localidad(2268,"MANZONE",2,1633),
					// Catamarca
					new Localidad(7268,"LA BREA",3,4700),
					new Localidad(7007,"LOS CASTILLOS",3,4711),
					new Localidad(12805,"EL SALTO",3,5260),
					// Entre Ríos
					new Localidad(5872,	"COLONIA PERFECCION",	6,	3260),
					new Localidad(15529,	"KILOMETRO 45",	6,	3116),
					new Localidad(5651,	"VILLA ROMERO",	6,	2824),
					// Mendoza
					new Localidad(13748,	"PORTILLO CRUZ DE PIEDRA",	8,	5569),
					new Localidad(13105,	"EMILIO NIETA",	8,	5590),
					new Localidad(3468,	"PUESTO QUEMADO",	8,	5435),
					// La Rioja
					new Localidad(17419,	"PIEDRA PINTADA",	9,	5361),
					new Localidad(16411,	"CUEVA DEL CHACHO",	9,	5386),
					new Localidad(10008,	"AIMOGASTA",	9,	5310),
					// Salta
					new Localidad(17350,	"PASCHA",	10,	4405),
					new Localidad(16925,	"CHUCULAQUI",	10,	4413),
					new Localidad(8116,	"EL QUEBRACHAL",	10,	4452),
					// Tucumán
					new Localidad(18607,	"LA COSTA PALAMPA",	15,	4176),
					new Localidad(9034,	"EL CARMEN",	15,	4128),
					new Localidad(9451,	"NASCHE",	15,	4152),
					// Chaco
					new Localidad(14539,	"COLONIA LA FILOMENA",	16,	3509),
					new Localidad(14044,	"CAMPO LAS PUERTAS",	16,	3541),
					new Localidad(7893,	"KILOMETRO 2 FCGB",	16,	3514),
					// Santa Cruz
					new Localidad(4979,	"TELLIER",	23,	9050),
					new Localidad(4955,	"LA GUARDIA",	23,	9015),
					new Localidad(5041,	"LA ASTURIANA",	23,	9040),
					// Tierra del Fuego
					new Localidad(22754,	"BASE ORCADAS",	24,	9411),
					new Localidad(2776,	"TOLHUIN",	24,	9420),
					new Localidad(21956,	"ESTANCIA ROSITA",	24,	9420)
			);
			persistirLista(daoLocalidad, localidades);
		}
	}
	
	private void checkEstadosPrestamos() {
		if (daoEstPrestamo.Count() == 0) {
			List<EstadoDePrestamo> estados = List.of(
					new EstadoDePrestamo(1, "En revisión"),
					new EstadoDePrestamo(2, "Aceptado"),
					new EstadoDePrestamo(3, "Rechazado")
			);
			persistirLista(daoEstPrestamo, estados);
		}
	}
	
	private void checkEstadosTransferencias() {
		if (daoEstTransferencia.Count() == 0) {
			List<EstadoDeTransferencia> estados = List.of(
					new EstadoDeTransferencia(1, "Aceptado"),
					new EstadoDeTransferencia(2, "Rechazado")
			);
			persistirLista(daoEstTransferencia, estados);
		}
	}
	
	private void checkTiposCuentas() {
		if (daoTiposCuenta.Count() == 0) {
			List<TipoDeCuenta> tipos = List.of(
					new TipoDeCuenta(1, "Caja de ahorro en pesos"),
					new TipoDeCuenta(2, "Caja de ahorro en dólares"),
					new TipoDeCuenta(3, "Cuenta corriente"),
					new TipoDeCuenta(4, "Cuenta corriente especial en pesos"),
					new TipoDeCuenta(5, "Cuenta corriente especial en dólares")
			);
			persistirLista(daoTiposCuenta, tipos);
		}
	}
	
	private void checkTiposMovimientos() {
		if (daoTiposMovimiento.Count() == 0) {
			List<TipoDeMovimiento> tipos = List.of(
					new TipoDeMovimiento(1, "Alta de cuenta"),
					new TipoDeMovimiento(2, "Alta de préstamo"),
					new TipoDeMovimiento(3, "Pago de préstamo"),
					new TipoDeMovimiento(4, "Transferencia")
			);
			persistirLista(daoTiposMovimiento, tipos);
		}
	}
	
	private void checkUserAdmin() {
		if (daoUsuario.Find("-").isEmpty()) {
			Usuario u = new Usuario();
			u.setUser("admin");
			u.setPassword("admin");
			u.setPerfil('A');
			u.setEstado(true);
			u.setApellido("-");
			u.setNombre("-");
			u.setDNI("-");
			u.setEmail("admin");
			u.setFechaDeNacimiento(LocalDate.now());
			u.setNacionalidad(daoPais.Find(1).get());
			u.setLocalidad(daoLocalidad.Find(7007).get());
			u.setDireccion("-");
			u.setSexo('M');
			
			daoUsuario.Save(u);
		}
	}

}
