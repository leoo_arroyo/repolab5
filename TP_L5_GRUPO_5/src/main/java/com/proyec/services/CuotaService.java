package com.proyec.services;

import java.util.List;

import com.proyec.entities.Cuota;
import com.proyec.services.generic.Service;
import com.proyec.utilweb.Response;

public interface CuotaService extends Service<Cuota, Integer>{
	
	public Response<List<Cuota>> ObtenerCuotas(Integer idPrestamo);

}
