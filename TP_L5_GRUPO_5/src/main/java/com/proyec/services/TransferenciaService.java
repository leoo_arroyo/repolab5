package com.proyec.services;

import java.util.List;

import com.proyec.entities.Transferencia;
import com.proyec.services.generic.Service;
import com.proyec.utilweb.Response;

public interface TransferenciaService extends Service<Transferencia, Integer> {
	
	Response<List<Transferencia>> ObtenerPorUsuario(String user);
	
}
