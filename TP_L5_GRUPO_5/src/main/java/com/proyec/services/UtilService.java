package com.proyec.services;

import java.util.List;

import com.proyec.entities.EstadoDeTransferencia;
import com.proyec.entities.Localidad;
import com.proyec.entities.Pais;
import com.proyec.entities.Provincia;
import com.proyec.entities.TipoDeCuenta;
import com.proyec.entities.TipoDeMovimiento;
import com.proyec.entities.structure.Menu;
import com.proyec.utilweb.Response;

public interface UtilService {
	
	Response<List<Menu>> ObtenerMenus(char perfil);
	
	Response<List<Pais>> ObtenerPaises();

	Response<List<Provincia>> ObtenerProvincias();
	
	Response<List<Localidad>> ObtenerLocalidades(Integer idProvincia);
	
	Response<List<TipoDeCuenta>> ObtenerTiposDeCuenta();
	
	Response<List<TipoDeMovimiento>> ObtenerTiposDeMovimiento();
	
	Response<List<EstadoDeTransferencia>> ObtenerEstadosDeTransferencia();
	
}
