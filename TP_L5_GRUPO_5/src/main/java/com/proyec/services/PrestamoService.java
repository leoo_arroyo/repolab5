package com.proyec.services;

import java.util.List;

import com.proyec.entities.Prestamo;
import com.proyec.services.generic.Service;
import com.proyec.utilweb.Response;

public interface PrestamoService extends Service<Prestamo, Integer> {
	
	Response<List<Prestamo>> ObtenerPorUsuario(String user);

}
