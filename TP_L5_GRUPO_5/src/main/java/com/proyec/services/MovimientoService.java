package com.proyec.services;

import java.util.List;

import com.proyec.entities.Movimiento;
import com.proyec.entities.Transferencia;
import com.proyec.services.generic.Service;
import com.proyec.utilweb.Response;

public interface MovimientoService extends Service<Movimiento, Integer> {
	
	Response<List<Movimiento>> ObtenerPorAlias(String alias);
	
	Response<List<Movimiento>> ObtenerPorCBU(String cbu);
	
	Response<List<Movimiento>> RegistrarPorTransferencia(Transferencia trs);

}
