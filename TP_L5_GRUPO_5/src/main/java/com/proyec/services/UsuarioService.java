package com.proyec.services;

import java.util.List;

import com.proyec.entities.Usuario;
import com.proyec.services.generic.Service;
import com.proyec.utilweb.Response;

public interface UsuarioService extends Service<Usuario, String> {
	
	Response<Usuario> Login(String user, String password);
	
	Response<Usuario> BuscarPorNick(String userNick);
	
	Response<List<Usuario>> ObtenerFiltradosPorUsuario(String user);
	
	Response<List<Usuario>> ObtenerFiltradosPorDNI(String dni);
	
}
