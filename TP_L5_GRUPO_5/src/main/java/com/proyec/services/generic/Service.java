package com.proyec.services.generic;

import java.util.List;

import com.proyec.utilweb.Response;

public interface Service<T, K> {
	
	Response<T> Guardar(T entity);
	
	Response<T> Actualizar(T entity);
	
	Response<T> Buscar(K key);
	
	Response<List<T>> ObtenerTodos();

}
