package com.proyec.services.generic;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.dao.generic.HibernateDAO;
import com.proyec.utilweb.Response;

public abstract class GenericService<T, K> implements Service<T, K> {
	
	@Autowired
	protected HibernateDAO<T> dao;

	@Override
	@Transactional
	public Response<T> Guardar(T entity) {
		dao.Save(entity);
		return new Response<>("OK", entity);
	}

	@Override
	@Transactional
	public Response<T> Actualizar(T entity) {
		dao.Update(entity);
		return new Response<>("OK", entity);
	}

	@Override
	@Transactional
	public Response<T> Buscar(K key) {
		Optional<T> opt = dao.Find(key);
		return opt.isPresent() ? new Response<>("OK", opt.get()) : new Response<>("ERROR: NOT FOUND", null);
	}

	@Override
	@Transactional
	public Response<List<T>> ObtenerTodos() {
		return new Response<List<T>>("OK", dao.FindAll());
	}

}