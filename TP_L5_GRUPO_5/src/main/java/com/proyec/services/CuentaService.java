package com.proyec.services;

import java.time.LocalDate;
import java.util.List;

import com.proyec.entities.Cuenta;
import com.proyec.services.generic.Service;
import com.proyec.utilweb.Response;

public interface CuentaService extends Service<Cuenta, String> {
	
	Response<Cuenta> ActualizarCuentaPrincipal(String user, String alias);
	
	Response<List<Cuenta>> ObtenerCuentas(String user);

	Response<List<Cuenta>> ObtenerCuentasActivas(String user);
	
	Response<List<Cuenta>> ObtenerPorFechas(LocalDate fechaInicio, LocalDate fechaFin);
	
	Response<Cuenta> ObtenerCuentaPrincipal(String user);
	
}
