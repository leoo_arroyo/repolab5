package com.proyec.services.impl;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.entities.EstadoDePrestamo;
import com.proyec.entities.Prestamo;
import com.proyec.services.PrestamoService;
import com.proyec.services.generic.GenericService;
import com.proyec.utilweb.Response;

@Service
public class PrestamoServiceImpl extends GenericService<Prestamo, Integer> implements PrestamoService {
	
	@Override
	public Response<Prestamo> Guardar(Prestamo entity) {
		final double intMinimo = 30;
        final double intMaximo = 50;
        
        DecimalFormat df = new DecimalFormat("#.##");
        df.setRoundingMode(RoundingMode.HALF_EVEN);
        
        // Calculo del interes
        String strInteres = df.format(intMinimo + (((intMaximo - intMinimo) / 11) * (entity.getPlazo() - 1)));
        entity.setInteres(Double.valueOf(strInteres.replace(',', '.')));
        
        // Calculo del importa de cada cuota
        Double interes = entity.getInteres();
        String importePorCuota = df.format((entity.getImporte() / entity.getPlazo()) * ((interes / 100) + 1));
        entity.setImportePorCuota(Double.valueOf(importePorCuota.replace(',', '.')));
        
        entity.setFechaSolicitud(LocalDate.now());
        entity.setEstado(new EstadoDePrestamo(1, null));
        
		return super.Guardar(entity);
	}

	@Override
	@Transactional
	public Response<List<Prestamo>> ObtenerPorUsuario(String user) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Prestamo.GetByUser", Map.of("user", user)));
	}

}
