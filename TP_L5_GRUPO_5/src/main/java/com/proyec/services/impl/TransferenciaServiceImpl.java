package com.proyec.services.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.dao.generic.HibernateDAO;
import com.proyec.entities.Cuenta;
import com.proyec.entities.EstadoDeTransferencia;
import com.proyec.entities.Transferencia;
import com.proyec.services.TransferenciaService;
import com.proyec.services.generic.GenericService;
import com.proyec.utilweb.Response;

@Service
public class TransferenciaServiceImpl extends GenericService<Transferencia, Integer> implements TransferenciaService {

	@Autowired
	HibernateDAO<Cuenta> daoCuentas;

	private Response<Transferencia> Validar(Transferencia trs) {
		Response<Transferencia> response = new Response<>("OK");

		List<Cuenta> result = daoCuentas.ExecuteNamedQuery("Cuenta.GetByAliasOrCBU",
				Map.of("key", trs.getCuentaDestino().getCBU()));

		if (result.isEmpty())
			response.addDetail("La cuenta de destino no existe");
		else
			trs.setCuentaDestino(result.get(0));

		result = daoCuentas.ExecuteNamedQuery("Cuenta.GetByAliasOrCBU",
				Map.of("key", trs.getCuentaOrigen().getCBU()));

		if (result.isEmpty())
			response.addDetail("La cuenta de origen no existe");
		else {
			trs.setCuentaOrigen(result.get(0));
			if ((result.get(0).getSaldo() - trs.getMonto()) < 0) {
				response.setMessage("SIN FONDOS");
				return response;
			}
		}

		if (!response.getDetails().isEmpty())
			response.setMessage("ERROR");

		return response;
	}

	@Override
	public Response<Transferencia> Guardar(Transferencia entity) {
		entity.setFecha(LocalDateTime.now());
		
		Response<Transferencia> response = Validar(entity);
		
		switch (response.getMessage()) {
		case "OK":
			entity.setEstado(new EstadoDeTransferencia(1, null));
			return super.Guardar(entity);
		case "ERROR":
			return response;
		case "SIN FONDOS":
			entity.setEstado(new EstadoDeTransferencia(2, null));
			response = super.Guardar(entity);
			if (response.getMessage().equals("OK")) {
				response.setMessage("ERROR");
				response.addDetail("La transferencia no pudo realizarse porque la cuenta no tiene los fondos suficientes");
			}
			return response;

		default:
			return new Response<>("ERROR");
		}
	}

	@Override
	@Transactional
	public Response<List<Transferencia>> ObtenerPorUsuario(String user) {
		List<Transferencia> result = dao.ExecuteNamedQuery("Transferencia.GetByUser", Map.of("user", user));
		return new Response<>("OK", result);
	}
}
