package com.proyec.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.entities.Usuario;
import com.proyec.services.UsuarioService;
import com.proyec.services.generic.GenericService;
import com.proyec.utilweb.Response;

@Service
public class UsuarioServiceImpl extends GenericService<Usuario, String> implements UsuarioService {

	private Response<Usuario> Validar(Usuario usuario, boolean esInsert) {
		Response<Usuario> response = new Response<>("OK");
		var users = dao.FindAll();

		if (esInsert)
			if (users.stream().anyMatch(e -> e.equals(usuario)))
				response.getDetails().add("- Ya existe un usuario con el DNI ingresado");
		
		if (usuario.getUser().equals("admin")) {
			response.setMessage("ERROR");
			return response.addDetail("Este usuario no se puede modificar");
		}
		
		if (users.stream().anyMatch(e -> e.getEmail().equals(usuario.getEmail()) && !e.equals(usuario)))
			response.getDetails().add("- El email ingresado ya pertenece a otra cuenta");
		
		if (users.stream().anyMatch(e -> e.getUser().equals(usuario.getUser()) && !e.equals(usuario)))
			response.getDetails().add("- El nombre de usuario ingresado no está disponible");

		if (!response.getDetails().isEmpty())
			response.setMessage("ERROR");
		
		return response;
	}

	@Override
	public Response<Usuario> Guardar(Usuario entity) {
		Response<Usuario> response = Validar(entity, true);
		if (response.getMessage().equals("ERROR"))
			return response;
		else {
			response = super.Guardar(entity);
			response.getData().setPassword("");
		}
		return response;
	}

	@Override
	public Response<Usuario> Actualizar(Usuario entity) {
		Response<Usuario> response = Validar(entity, false);
		if (response.getMessage().equals("ERROR"))
			return response;
		else {
			if (entity.getPassword().isEmpty())
				entity.setPassword(super.Buscar(entity.getDNI()).getData().getPassword());
			
			response = super.Actualizar(entity);
			response.getData().setPassword("");
		}
		return response;
	}

	@Override
	@Transactional
	public Response<Usuario> Login(String user, String password) {
		Response<Usuario> response = new Response<>();
		List<Usuario> result = dao.ExecuteNamedQuery("Usuario.Login", Map.of("user", user, "password", password));
		if (result.size() == 0) {
			response.setMessage("ERROR");
			response.getDetails().add("Usuario o contraseña incorrectos.");
		}
		else if (!result.get(0).getEstado()) {
			response.setMessage("ERROR");
			response.getDetails().add("Usuario deshabilitado.");
		}
		else {
			response.setMessage("OK");
			response.setData(result.get(0));
			response.getData().setPassword("");
		}
		return response;
	}
	
	@Override
	@Transactional
	public Response<Usuario> BuscarPorNick(String userNick) {
		Response<Usuario> response = new Response<>();
		List<Usuario> result = dao.ExecuteNamedQuery("Usuario.GetByNick", Map.of("nick", userNick));
		if (result.isEmpty())
			response.setMessage("ERROR: NOT FOUND");
		else {
			response.setMessage("OK");
			response.setData(result.get(0));
		}
		return response;
	}

	@Override
	@Transactional
	public Response<List<Usuario>> ObtenerFiltradosPorUsuario(String user) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Usuario.FilterByUser", Map.of("user", user)));
	}

	@Override
	@Transactional
	public Response<List<Usuario>> ObtenerFiltradosPorDNI(String dni) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Usuario.FilterByDNI", Map.of("dni", dni)));
	}

}
