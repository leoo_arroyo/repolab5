package com.proyec.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.entities.Cuota;
import com.proyec.services.CuotaService;
import com.proyec.services.generic.GenericService;
import com.proyec.utilweb.Response;

@Service
public class CuotaServiceImpl extends GenericService<Cuota, Integer> implements CuotaService {

	@Override
	@Transactional
	public Response<List<Cuota>> ObtenerCuotas(Integer idPrestamo) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Cuota.GetByIdPrestamo", Map.of("idPrestamo", idPrestamo)));
	}
	
}
