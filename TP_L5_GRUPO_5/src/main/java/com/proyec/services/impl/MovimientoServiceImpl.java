package com.proyec.services.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.dao.generic.HibernateDAO;
import com.proyec.entities.Cuenta;
import com.proyec.entities.Movimiento;
import com.proyec.entities.TipoDeMovimiento;
import com.proyec.entities.Transferencia;
import com.proyec.services.MovimientoService;
import com.proyec.services.generic.GenericService;
import com.proyec.utilweb.Response;

@Service
public class MovimientoServiceImpl extends GenericService<Movimiento, Integer> implements MovimientoService {

	@Autowired
	HibernateDAO<Cuenta> daoCuentas;

	@Override
	public Response<Movimiento> Guardar(Movimiento entity) {
		entity.setFecha(LocalDateTime.now());
		return super.Guardar(entity);
	}

	@Override
	@Transactional
	public Response<List<Movimiento>> ObtenerPorAlias(String alias) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Movimiento.GetByAlias", Map.of("alias", alias)));
	}

	@Override
	@Transactional
	public Response<List<Movimiento>> ObtenerPorCBU(String cbu) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Movimiento.GetByCBU", Map.of("cbu", cbu)));
	}

	@Override
	@Transactional
	public Response<List<Movimiento>> RegistrarPorTransferencia(Transferencia trs) {
		Cuenta cOrigen = daoCuentas.Find(trs.getCuentaOrigen().getCBU()).get();
		Cuenta cDestino = daoCuentas.Find(trs.getCuentaDestino().getCBU()).get();

		Movimiento mNegativo = new Movimiento();
		Movimiento mPositivo = new Movimiento();

		mNegativo.setCuenta(trs.getCuentaOrigen());
		mNegativo.setImporte(trs.getMonto());
		mNegativo.setFecha(LocalDateTime.now());
		mNegativo.setDetalle(String.format("Transferencia a %s", trs.getCuentaDestino().getCBU()));
		mNegativo.setTipoMovimiento(new TipoDeMovimiento(4, null));
		mNegativo.setSigno('-');

		Double nuevoSaldoOrigen = cOrigen.getSaldo() - mNegativo.getImporte();
		cOrigen.setSaldo(nuevoSaldoOrigen);

		mPositivo.setCuenta(trs.getCuentaDestino());
		mPositivo.setImporte(trs.getMonto());
		mPositivo.setFecha(LocalDateTime.now());
		mPositivo.setDetalle(String.format("Transferencia de %s", trs.getCuentaOrigen().getCBU()));
		mPositivo.setTipoMovimiento(new TipoDeMovimiento(4, null));
		mPositivo.setSigno('+');

		Double nuevoSaldoDestino = cDestino.getSaldo() + mPositivo.getImporte();
		cDestino.setSaldo(nuevoSaldoDestino);

		try {
			daoCuentas.Update(cOrigen);
			daoCuentas.Update(cDestino);
		} catch (Exception e) {
			e.printStackTrace();
			return new Response<List<Movimiento>>("ERROR")
					.addDetail("Hubo un error al actualizar el estado de las cuentas");
		}

		if (this.Guardar(mPositivo).getMessage().equals("OK") && this.Guardar(mNegativo).getMessage().equals("OK")) {
			return new Response<>("OK", List.of(mNegativo, mPositivo));
		} else {
			var response = new Response<List<Movimiento>>();
			response.setMessage("ERROR");
			response.getDetails().add("Hubo un error al registrar los movimientos");
			return response;
		}
	}

}
