package com.proyec.services.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.dao.generic.HibernateDAO;
import com.proyec.entities.Cuenta;
import com.proyec.entities.Usuario;
import com.proyec.services.CuentaService;
import com.proyec.services.generic.GenericService;
import com.proyec.utilweb.Response;

@Service
public class CuentaServiceImpl extends GenericService<Cuenta, String> implements CuentaService {

	@Autowired
	HibernateDAO<Usuario> daoUsuarios;

	private Response<Cuenta> Validar(Cuenta cuenta, boolean esInsert) {
		Response<Cuenta> response = new Response<>("OK");
		List<Cuenta> result = null;

		Optional<Usuario> user = daoUsuarios.Find(cuenta.getUsuario().getDNI());

		if (user.isEmpty())
			response.getDetails().add("- El usuario ingresado no existe");
		else {
			if (user.get().getUser().equals("admin")) {
				response.setMessage("ERROR");
				return response.addDetail("No se puede asignar cuentas a este usuario");
			}
		}

		result = this.ObtenerCuentas(cuenta.getUsuario().getDNI()).getData();
		if (result.size() == 4 && esInsert)
			response.getDetails().add("- El usuario ya alcanzó el límite de cuentas");

		if (esInsert)
			if (super.Buscar(cuenta.getCBU()).getMessage().equals("OK"))
				response.getDetails().add("- El CBU ingresado no está disponible");

		result = dao.ExecuteNamedQuery("Cuenta.GetByAlias", Map.of("alias", cuenta.getAlias()));
		if (!result.isEmpty())
			if (!result.get(0).equals(cuenta))
				response.getDetails().add("- El alias ingresado no está disponible");

		result = dao.ExecuteNamedQuery("Cuenta.GetByNumero", Map.of("numero", cuenta.getNumeroDeCuenta()));
		if (!result.isEmpty())
			if (!result.get(0).equals(cuenta))
				response.getDetails().add("- El número de cuenta ingresado no está disponible");

		if (!response.getDetails().isEmpty())
			response.setMessage("ERROR");

		return response;
	}

	@Override
	@Transactional
	public Response<Cuenta> Guardar(Cuenta entity) {
		Response<Cuenta> response = Validar(entity, true);
		if (response.getMessage().equals("ERROR"))
			return response;
		else {
			int count = this.ObtenerCuentas(entity.getUsuario().getDNI()).getData().size();
			entity.setEsPrincipal((count == 0));
			entity.setFechaCreacion(LocalDate.now());
			
			response = super.Guardar(entity);
		}
		return response;
	}

	@Override
	@Transactional
	public Response<Cuenta> Actualizar(Cuenta entity) {
		Response<Cuenta> response = Validar(entity, false);
		if (response.getMessage().equals("ERROR")) {
			return response;
		} else {
			Cuenta current = super.Buscar(entity.getCBU()).getData();
			entity.setFechaCreacion(current.getFechaCreacion());
			entity.setEsPrincipal(current.getEsPrincipal());

			response = super.Actualizar(entity);
		}
		return response;
	}

	@Override
	@Transactional
	public Response<Cuenta> ActualizarCuentaPrincipal(String user, String alias) {
		Cuenta principalNueva = this.Buscar(alias).getData();

		List<Cuenta> result = this.ObtenerCuentas(user).getData();
		if (!result.contains(principalNueva)) {
			return new Response<Cuenta>("ERROR").addDetail("La cuenta no pertence al usuario");
		}

		Cuenta principalActual = this.ObtenerCuentaPrincipal(user).getData();

		principalActual.setEsPrincipal(false);
		principalNueva.setEsPrincipal(true);

		if (super.Actualizar(principalActual).getMessage().equals("OK")
				&& super.Actualizar(principalNueva).getMessage().equals("OK"))
			return new Response<>("OK");
		else
			return new Response<Cuenta>("ERROR").addDetail("Hubo un error al actualizar el estado de las cuentas");
	}

	@Override
	public Response<Cuenta> Buscar(String key) {
		List<Cuenta> result = dao.ExecuteNamedQuery("Cuenta.GetByAliasOrCBU", Map.of("key", key));
		return (result.isEmpty()) ? new Response<>("ERROR: NOT FOUND", null) : new Response<>("OK", result.get(0));
	}

	@Override
	@Transactional
	public Response<List<Cuenta>> ObtenerCuentas(String user) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Cuenta.GetByUsuario", Map.of("key", user)));
	}
	
	@Override
	@Transactional
	public Response<List<Cuenta>> ObtenerCuentasActivas(String user) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Cuenta.GetByUsuarioActive", Map.of("key", user)));
	}

	@Override
	@Transactional
	public Response<Cuenta> ObtenerCuentaPrincipal(String user) {
		List<Cuenta> result = dao.ExecuteNamedQuery("Cuenta.GetCuentaPrincipal", Map.of("user", user));
		if (result.isEmpty())
			return new Response<Cuenta>("ERROR").addDetail("Sin cuentas disponibles");
		else
			return new Response<Cuenta>("OK", result.get(0));
	}

	@Override
	@Transactional
	public Response<List<Cuenta>> ObtenerPorFechas(LocalDate fechaInicio, LocalDate fechaFin) {
		return new Response<>("OK", dao.ExecuteNamedQuery("Cuenta.GetByIntervaloFecha", Map.of("fInicio", fechaInicio, "fFin", fechaFin)));
	}
}
