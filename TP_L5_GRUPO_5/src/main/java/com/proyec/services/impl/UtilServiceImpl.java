package com.proyec.services.impl;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.proyec.dao.generic.HibernateDAO;
import com.proyec.entities.EstadoDeTransferencia;
import com.proyec.entities.Localidad;
import com.proyec.entities.Pais;
import com.proyec.entities.Provincia;
import com.proyec.entities.TipoDeCuenta;
import com.proyec.entities.TipoDeMovimiento;
import com.proyec.entities.structure.Menu;
import com.proyec.services.UtilService;
import com.proyec.utilweb.Response;

@Service
public class UtilServiceImpl implements UtilService {

	@Autowired
	HibernateDAO<Menu> daoMenus;

	@Autowired
	HibernateDAO<Pais> daoPaises;

	@Autowired
	HibernateDAO<Provincia> daoProvincias;

	@Autowired
	HibernateDAO<Localidad> daoLocalidades;

	@Autowired
	HibernateDAO<TipoDeCuenta> daoTiposCuenta;
	
	@Autowired
	HibernateDAO<TipoDeMovimiento> daoTiposMovimiento;
	
	@Autowired
	HibernateDAO<EstadoDeTransferencia> daoEstTransferencia;

	@Override
	@Transactional
	public Response<List<Menu>> ObtenerMenus(char perfil) {
		List<Menu> result = daoMenus.ExecuteNamedQuery("Menu.GetByProfile", Map.of("perfil", perfil));
		result.forEach(m -> m.getPaginas().sort((p1, p2) -> p1.getNombre().compareTo(p2.getNombre())));
		return new Response<>("OK", result);
	}

	@Override
	@Transactional
	public Response<List<Localidad>> ObtenerLocalidades(Integer idProvincia) {
		return new Response<>("OK",
				daoLocalidades.ExecuteNamedQuery("Localidad.ObtenerPorProvincia", Map.of("idProvincia", idProvincia)));
	}

	@Override
	@Transactional
	public Response<List<Pais>> ObtenerPaises() {
		return new Response<>("OK", daoPaises.FindAll());
	}

	@Override
	@Transactional
	public Response<List<Provincia>> ObtenerProvincias() {
		return new Response<>("OK", daoProvincias.ExecuteNamedQuery("Provincia.ObtenerTodos", null));
	}

	@Override
	@Transactional
	public Response<List<TipoDeCuenta>> ObtenerTiposDeCuenta() {
		return new Response<>("OK", daoTiposCuenta.FindAll());
	}
	
	@Override
	@Transactional
	public Response<List<TipoDeMovimiento>> ObtenerTiposDeMovimiento() {
		return new Response<>("OK", daoTiposMovimiento.FindAll());
	}

	@Override
	@Transactional
	public Response<List<EstadoDeTransferencia>> ObtenerEstadosDeTransferencia() {
		return new Response<>("OK", daoEstTransferencia.FindAll());
	}

}
