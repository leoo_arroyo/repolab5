package com.proyec.dto;

import java.io.Serializable;

@SuppressWarnings("serial")
public class MovimientoDTO implements Serializable {
	
	private String fecha;
	private String detalle;
	private String importe;
	private String signo;
	
	public MovimientoDTO() {
		super();
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getSigno() {
		return signo;
	}

	public void setSigno(String signo) {
		this.signo = signo;
	}

}
