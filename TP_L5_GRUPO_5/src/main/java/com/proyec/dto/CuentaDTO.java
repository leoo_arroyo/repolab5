package com.proyec.dto;

import java.util.Date;

import com.proyec.entities.TipoDeCuenta;

public class CuentaDTO {
	
	private String cbu;
	private String alias;
	private String numero;
	private Date fechaCreacion;
	private TipoDeCuenta tipo;
	private Double saldo;
	private Boolean estado;
	private String user;
	
	public CuentaDTO() {
		super();
	}

	public String getCbu() {
		return cbu;
	}

	public void setCbu(String cbu) {
		this.cbu = cbu;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public Date getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public TipoDeCuenta getTipo() {
		return tipo;
	}

	public void setTipo(TipoDeCuenta tipo) {
		this.tipo = tipo;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

}
