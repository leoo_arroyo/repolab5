package com.proyec.entities;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.format.annotation.DateTimeFormat.ISO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@SuppressWarnings("serial")
@Entity
@Table(name = "tbl_usuarios")
@NamedQueries({
	@NamedQuery(name = "Usuario.Login", query = "SELECT u FROM Usuario u where (u.email = :user OR u.user = :user) AND u.password = :password"),
	@NamedQuery(name = "Usuario.GetByEmail", query = "SELECT u FROM Usuario u where u.email = :email"),
	@NamedQuery(name = "Usuario.GetByNick", query = "SELECT u FROM Usuario u where u.user = :nick"),
	@NamedQuery(name = "Usuario.FilterByUser", query = "SELECT u FROM Usuario u where u.user like concat('%',:user,'%')"),
	@NamedQuery(name = "Usuario.FilterByDNI", query = "SELECT u FROM Usuario u where u.DNI like concat('%',:dni,'%')")
})
public class Usuario implements Serializable {
	
	@Id
	@Column(name = "DNI", length = 12)
	private String DNI;

	@Column(name = "user", length = 50)
	private String user;

	@Column(length = 40)
	private String password;

	@Column(length = 50, nullable = false)
	private String nombre;

	@Column(length = 50, nullable = false)
	private String apellido;

	@Column(length = 50, nullable = false, unique = true)
	private String email;

	private Character sexo;

	@Column(nullable = false)
	@DateTimeFormat(iso = ISO.DATE)
	private LocalDate fechaDeNacimiento;

	@Column(length = 60)
	private String direccion;

	@ManyToOne
	@JoinColumn(name = "IdPais", nullable = false, referencedColumnName = "Id")
	private Pais nacionalidad;

	@ManyToOne
	@JoinColumn(name = "IdLocalidad", referencedColumnName = "IdLocalidad")
	private Localidad localidad;

	@Column(nullable = false)
	private Boolean estado;

	@OneToMany(mappedBy = "usuario", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JsonIgnoreProperties(value = "usuario")
	private Set<Cuenta> cuentas = new HashSet<>();
	
	@Column(name = "Perfil", nullable = false)
	private char perfil;

	public Usuario() {
		super();
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDNI() {
		return DNI;
	}

	public void setDNI(String dNI) {
		DNI = dNI;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Character getSexo() {
		return sexo;
	}

	public void setSexo(Character sexo) {
		this.sexo = sexo;
	}

	public LocalDate getFechaDeNacimiento() {
		return fechaDeNacimiento;
	}

	public void setFechaDeNacimiento(LocalDate fechaDeNacimiento) {
		this.fechaDeNacimiento = fechaDeNacimiento;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public Pais getNacionalidad() {
		return nacionalidad;
	}
	
	public void setNacionalidad(Pais nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public Localidad getLocalidad() {
		return localidad;
	}

	public void setLocalidad(Localidad localidad) {
		this.localidad = localidad;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Set<Cuenta> getCuentas() {
		return cuentas;
	}

	public void setCuentas(Set<Cuenta> cuentas) {
		this.cuentas = cuentas;
	}

	public void addCuenta(Cuenta cuenta) {
		cuenta.setUsuario(this);
		this.cuentas.add(cuenta);
	}
	public char getPerfil() {
		return perfil;
	}
	
	public void setPerfil(char perfil) {
		this.perfil = perfil;
	}
	
	@Override
	public boolean equals(Object obj) {
		return this.DNI.equals(((Usuario) obj).DNI);
	}

}
