package com.proyec.entities.structure;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_menus")
@NamedQuery(name = "Menu.GetByProfile", query = "SELECT m FROM Menu m WHERE m.perfil = :perfil ORDER BY m.nombre asc")
public class Menu {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMenu;
	private String nombre;
	
	@Column(name = "Perfil")
	private char perfil;
	
	@OneToMany(mappedBy = "menu", fetch = FetchType.EAGER, cascade = CascadeType.ALL)
	private List<Pagina> paginas = new ArrayList<>();
	
	public Menu() {
		super();
	}

	public Integer getIdMenu() {
		return idMenu;
	}

	public void setIdMenu(Integer idMenu) {
		this.idMenu = idMenu;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public char getPerfil() {
		return perfil;
	}
	
	public void setPerfil(char perfil) {
		this.perfil = perfil;
	}
	
	public List<Pagina> getPaginas() {
		return paginas;
	}
	public void setPaginas(List<Pagina> paginas) {
		this.paginas = paginas;
	}
	
	public void addPagina(Pagina pagina) {
		pagina.setMenu(this);
		paginas.add(pagina);
	}
	
}