package com.proyec.entities;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_prestamos")
@NamedQueries({
	@NamedQuery(name = "Prestamo.GetByUser", query = "SELECT p FROM Prestamo p WHERE p.usuario.user = :user")
})
public class Prestamo {
	
	@Id
	@Column(name = "IdPrestamo")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idPrestamo;
	
	@ManyToOne
	@JoinColumn(name = "UsuarioDNI", referencedColumnName = "DNI", nullable = false)
	private Usuario usuario;
	
	@Column(nullable = false)
	private LocalDate fechaSolicitud;
	
	private LocalDate fechaAprobacion;
	
	@Column(nullable = false)
	private Double importe;
	
	@Column(nullable = false)
	private Integer plazo;
	
	private Double interes;
	
	@Column(name="ImportePorCuota", columnDefinition="Decimal(10,2)", nullable = false)
	private Double importePorCuota;
	
	@ManyToOne
	@JoinColumn(name = "CBU", referencedColumnName = "CBU", nullable = false)
	private Cuenta cuenta;
	
	@ManyToOne
	@JoinColumn(name = "Estado", referencedColumnName = "id", nullable = false)
	private EstadoDePrestamo estado;
	
	public Prestamo() {
		super();
	}

	public Prestamo(Integer idPrestamo) {
		super();
		this.idPrestamo = idPrestamo;
	}

	public Integer getIdPrestamo() {
		return idPrestamo;
	}

	public void setIdPrestamo(Integer idPrestamo) {
		this.idPrestamo = idPrestamo;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public LocalDate getFechaSolicitud() {
		return fechaSolicitud;
	}

	public void setFechaSolicitud(LocalDate fechaSolicitud) {
		this.fechaSolicitud = fechaSolicitud;
	}

	public LocalDate getFechaAprobacion() {
		return fechaAprobacion;
	}

	public void setFechaAprobacion(LocalDate fechaAprobacion) {
		this.fechaAprobacion = fechaAprobacion;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}

	public Integer getPlazo() {
		return plazo;
	}

	public void setPlazo(Integer plazo) {
		this.plazo = plazo;
	}

	public Double getInteres() {
		return interes;
	}

	public void setInteres(Double interes) {
		this.interes = interes;
	}

	public Double getImportePorCuota() {
		return importePorCuota;
	}

	public void setImportePorCuota(Double importePorCuota) {
		this.importePorCuota = importePorCuota;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public EstadoDePrestamo getEstado() {
		return estado;
	}

	public void setEstado(EstadoDePrestamo estado) {
		this.estado = estado;
	}

}
