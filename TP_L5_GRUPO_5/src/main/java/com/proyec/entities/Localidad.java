package com.proyec.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_localidades")
@NamedQuery(name = "Localidad.ObtenerPorProvincia", query = "SELECT l FROM Localidad l where l.provincia.idProvincia = :idProvincia ORDER BY l.nombre")
public class Localidad {

	@Id
	@Column(name = "IdLocalidad")
	private Integer idLocalidad;

	@Column(name = "NombreLocalidad", length = 80, nullable = false)
	private String nombre;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "IdProvincia")
	private Provincia provincia;

	@Column(name = "CodigoPostal")
	private Integer codigoPostal;

	public Localidad() {
		super();
	}

	public Localidad(Integer idLocalidad, String nombre) {
		super();
		this.idLocalidad = idLocalidad;
		this.nombre = nombre;
	}

	public Localidad(Integer idLocalidad, String nombre, Integer idProvincia, Integer codigoPostal) {
		super();
		this.idLocalidad = idLocalidad;
		this.nombre = nombre;
		this.provincia = new Provincia(idProvincia, null);
		this.codigoPostal = codigoPostal;
	}

	public Integer getIdLocalidad() {
		return idLocalidad;
	}

	public void setIdLocalidad(Integer idLocalidad) {
		this.idLocalidad = idLocalidad;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Provincia getProvincia() {
		return provincia;
	}

	public void setProvincia(Provincia provincia) {
		this.provincia = provincia;
	}

	public Integer getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(Integer codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	@Override
	public String toString() {
		return "Localidad [idLocalidad=" + idLocalidad + ", nombre=" + nombre + ", provincia=" + provincia
				+ ", codigoPostal=" + codigoPostal + "]";
	}

}
