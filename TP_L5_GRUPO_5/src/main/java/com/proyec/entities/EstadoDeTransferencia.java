package com.proyec.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_transferencias_estados")
public class EstadoDeTransferencia {
	
	@Id
	private Integer id;
	
	@Column(length = 40, nullable = false)
	private String descripcion;
	
	public EstadoDeTransferencia() {
		super();
	}

	public EstadoDeTransferencia(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}