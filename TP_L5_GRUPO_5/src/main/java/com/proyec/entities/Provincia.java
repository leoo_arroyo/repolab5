package com.proyec.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_provincias")
@NamedQuery(name = "Provincia.ObtenerTodos",
query = "SELECT p FROM Provincia p ORDER BY p.nombre")
public class Provincia {

	@Id
	@Column(name = "IdProvincia")
	private Integer idProvincia;

	@Column(name = "NombreProvincia", length = 30, nullable = false)
	private String nombre;
	
	@Column(name = "CodigoProvincia", length = 4)
	private String codigo;
	
	public Provincia() {
		super();
	}

	public Provincia(Integer idProvincia, String nombre) {
		super();
		this.idProvincia = idProvincia;
		this.nombre = nombre;
	}

	public Provincia(Integer idProvincia, String nombre, String codigo) {
		super();
		this.idProvincia = idProvincia;
		this.nombre = nombre;
		this.codigo = codigo;
	}

	public Integer getIdProvincia() {
		return idProvincia;
	}

	public void setIdProvincia(Integer idProvincia) {
		this.idProvincia = idProvincia;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
}
