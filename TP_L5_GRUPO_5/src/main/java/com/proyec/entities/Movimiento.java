package com.proyec.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_movimientos")
@NamedQueries({
	@NamedQuery(name = "Movimiento.GetByAlias", query = "SELECT m FROM Movimiento m WHERE m.cuenta.alias = :alias ORDER BY m.fecha desc"),
	@NamedQuery(name = "Movimiento.GetByCBU", query = "SELECT m FROM Movimiento m WHERE m.cuenta.CBU = :cbu ORDER BY m.fecha desc")
})
public class Movimiento {
	
	@Id
	@Column(name = "IdMovimiento")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idMovimiento;
	
	@ManyToOne
	@JoinColumn(name = "CBU", nullable = false)
	private Cuenta cuenta;
	
	@Column(name = "Importe", nullable = false)
	private Double importe;
	
	@Column(name = "Signo", nullable = false)
	private Character signo;
	
	@Column(name = "Fecha", nullable = false)
	private LocalDateTime fecha;
	
	@Column(name = "Detalle", length = 250)
	private String detalle;
	
	@ManyToOne
	@JoinColumn(name = "Tipo", referencedColumnName = "id", nullable = false)
	private TipoDeMovimiento tipoMovimiento;
	
	public Movimiento() {
		super();
	}

	public Movimiento(Integer id) {
		super();
		this.idMovimiento = id;
	}

	public Integer getId() {
		return idMovimiento;
	}

	public void setId(Integer id) {
		this.idMovimiento = id;
	}

	public Cuenta getCuenta() {
		return cuenta;
	}

	public void setCuenta(Cuenta cuenta) {
		this.cuenta = cuenta;
	}

	public Double getImporte() {
		return importe;
	}

	public void setImporte(Double importe) {
		this.importe = importe;
	}
	
	public Character getSigno() {
		return signo;
	}
	
	public void setSigno(Character signo) {
		this.signo = signo;
	}
	
	public LocalDateTime getFecha() {
		return fecha;
	}
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public String getDetalle() {
		return detalle;
	}

	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}

	public TipoDeMovimiento getTipoMovimiento() {
		return tipoMovimiento;
	}

	public void setTipoMovimiento(TipoDeMovimiento tipoMovimiento) {
		this.tipoMovimiento = tipoMovimiento;
	}

}
