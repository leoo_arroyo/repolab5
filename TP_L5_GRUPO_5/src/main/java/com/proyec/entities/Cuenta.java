package com.proyec.entities;

import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tbl_cuentas")
@NamedQueries({
		@NamedQuery(name = "Cuenta.GetByUsuario", query = "SELECT c FROM Cuenta c WHERE (c.usuario.DNI = :key OR c.usuario.user = :key) ORDER BY c.alias"),
		@NamedQuery(name = "Cuenta.GetByUsuarioActive", query = "SELECT c FROM Cuenta c WHERE (c.usuario.DNI = :key OR c.usuario.user = :key) AND c.estado = 1 ORDER BY c.alias"),
		@NamedQuery(name = "Cuenta.GetByAliasOrCBU", query = "SELECT c FROM Cuenta c WHERE (c.alias = :key OR c.CBU = :key)"),
		@NamedQuery(name = "Cuenta.GetByAlias", query = "SELECT c FROM Cuenta c WHERE c.alias = :alias"),
		@NamedQuery(name = "Cuenta.GetByNumero", query = "SELECT c FROM Cuenta c WHERE c.numeroDeCuenta = :numero"),
		@NamedQuery(name = "Cuenta.GetByIntervaloFecha", query = "SELECT c FROM Cuenta c WHERE c.fechaCreacion BETWEEN :fInicio AND :fFin"),
		@NamedQuery(name = "Cuenta.GetCuentaPrincipal", query = "SELECT c FROM Cuenta c WHERE (c.usuario.user = :user AND c.esPrincipal = true)") })
public class Cuenta {

	@Id
	@Column(name = "CBU", length = 26)
	private String CBU;

	@Column(name = "Alias", unique = true, length = 50, nullable = false)
	private String alias;

	@Column(name = "Numero", unique = true, length = 23)
	private String numeroDeCuenta;

	@Column(name = "FechaCreacion", nullable = false)
	private LocalDate fechaCreacion;

	@ManyToOne
	@JoinColumn(name = "TipoDeCuenta", referencedColumnName = "id", nullable = false)
	private TipoDeCuenta tipoDeCuenta;

	@Column(name = "Saldo", columnDefinition = "Decimal(10,2)", nullable = false)
	private Double saldo;

	@Column(name = "EsPrincipal", nullable = false)
	private Boolean esPrincipal;

	@Column(name = "Estado", nullable = false)
	private Boolean estado;

	@ManyToOne
	@JoinColumn(name = "UsuarioDNI", referencedColumnName = "DNI", nullable = false)
	@JsonIgnoreProperties(value = "cuentas")
	private Usuario usuario;

	public Cuenta() {
		super();
	}

	public Cuenta(String CBU) {
		super();
		this.CBU = CBU;
	}

	public String getCBU() {
		return CBU;
	}

	public void setCBU(String cBU) {
		CBU = cBU;
	}

	public String getNumeroDeCuenta() {
		return numeroDeCuenta;
	}

	public void setNumeroDeCuenta(String numeroDeCuenta) {
		this.numeroDeCuenta = numeroDeCuenta;
	}

	public LocalDate getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(LocalDate fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public TipoDeCuenta getTipoDeCuenta() {
		return tipoDeCuenta;
	}

	public void setTipoDeCuenta(TipoDeCuenta tipoDeCuenta) {
		this.tipoDeCuenta = tipoDeCuenta;
	}

	public String getAlias() {
		return alias;
	}

	public void setAlias(String alias) {
		this.alias = alias;
	}

	public Double getSaldo() {
		return saldo;
	}

	public void setSaldo(Double saldo) {
		this.saldo = saldo;
	}

	public Boolean getEsPrincipal() {
		return esPrincipal;
	}

	public void setEsPrincipal(Boolean esPrincipal) {
		this.esPrincipal = esPrincipal;
	}

	public Boolean getEstado() {
		return estado;
	}

	public void setEstado(Boolean estado) {
		this.estado = estado;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	@Override
	public boolean equals(Object obj) {
		return this.CBU.equals(((Cuenta) obj).CBU);
	}

	@Override
	public String toString() {
		return "Cuenta [CBU=" + CBU + ", alias=" + alias + ", numeroDeCuenta=" + numeroDeCuenta + ", fechaCreacion="
				+ fechaCreacion + ", tipoDeCuenta=" + tipoDeCuenta + ", saldo=" + saldo + ", estado=" + estado
				+ ", usuario=" + usuario.getUser() + "]";
	}
}
