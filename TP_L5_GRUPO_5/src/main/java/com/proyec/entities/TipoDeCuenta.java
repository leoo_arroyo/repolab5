package com.proyec.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_tipos_de_cuenta")
public class TipoDeCuenta {
	
	@Id
	private Integer id;
	
	@Column(length = 40, nullable = false)
	private String descripcion;
	
	public TipoDeCuenta() {
		super();
	}

	public TipoDeCuenta(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
