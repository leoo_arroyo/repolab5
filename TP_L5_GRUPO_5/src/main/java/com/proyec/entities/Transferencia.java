package com.proyec.entities;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "tbl_transferencias")
@NamedQuery(name = "Transferencia.GetByUser", query = "SELECT t FROM Transferencia t where (t.cuentaOrigen.usuario.user = :user OR t.cuentaDestino.usuario.user = :user)")
public class Transferencia {
	
	@Id
	@Column(name = "IdTransferencia")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idTransferencia;
	
	@ManyToOne
	@JoinColumn(name = "CBUCuentaOrigen", referencedColumnName = "CBU", nullable = false)
	private Cuenta cuentaOrigen;
	
	@ManyToOne
	@JoinColumn(name = "CBUCuentaDestino", referencedColumnName = "CBU", nullable = false)
	private Cuenta cuentaDestino;
	
	@Column(nullable = false)
	private Double monto;
	
	@Column(nullable = false)
	private LocalDateTime fecha;
	
	@ManyToOne
	@JoinColumn(name = "Estado", referencedColumnName = "id", nullable = false)
	private EstadoDeTransferencia estado;
	
	public Transferencia() {
		super();
	}

	public Transferencia(Integer idTransferencia) {
		super();
		this.idTransferencia = idTransferencia;
	}

	public Integer getIdTransferencia() {
		return idTransferencia;
	}

	public void setIdTransferencia(Integer idTransferencia) {
		this.idTransferencia = idTransferencia;
	}

	public Cuenta getCuentaOrigen() {
		return cuentaOrigen;
	}

	public void setCuentaOrigen(Cuenta cuentaOrigen) {
		this.cuentaOrigen = cuentaOrigen;
	}

	public Cuenta getCuentaDestino() {
		return cuentaDestino;
	}

	public void setCuentaDestino(Cuenta cuentaDestino) {
		this.cuentaDestino = cuentaDestino;
	}

	public Double getMonto() {
		return monto;
	}

	public void setMonto(Double monto) {
		this.monto = monto;
	}

	public LocalDateTime getFecha() {
		return fecha;
	}
	
	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}
	
	public EstadoDeTransferencia getEstado() {
		return estado;
	}
	
	public void setEstado(EstadoDeTransferencia estado) {
		this.estado = estado;
	}

	@Override
	public String toString() {
		return "Transferencia [idTransferencia=" + idTransferencia + ", cuentaOrigen=" + cuentaOrigen.getCBU()
				+ ", cuentaDestino=" + cuentaDestino.getCBU() + ", monto=" + monto + ", fecha=" + fecha + ", estado=" + estado
				+ "]";
	}
	
	
}
