package com.proyec.entities;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "tbl_cuotas")
@NamedQueries({
	@NamedQuery(name = "Cuota.GetByIdPrestamo", query = "SELECT c FROM Cuota c WHERE c.prestamo.idPrestamo = :idPrestamo")
})
public class Cuota {
	
	@Id
	@Column(name = "IdCuota")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer idCuota;
	
	@ManyToOne
	@JoinColumn(name = "IdPrestamo", nullable = false)
	@JsonIgnoreProperties(value = {"usuario", "cuenta"})
	private Prestamo prestamo;
	
	@Column(name="Numero", nullable = false)
	private Integer numeroDeCuota;
	
	@Column(name="FechaDePago", nullable = true)
	private LocalDate fechaDePago;
	
	@Column(name="EstadoDePago", nullable = false)
	private Boolean estadoDePago;
	
	public Cuota() {
		super();
	}

	public Cuota(Integer idCuota) {
		super();
		this.idCuota = idCuota;
	}

	public Integer getIdCuota() {
		return idCuota;
	}

	public void setIdCuota(Integer idCuota) {
		this.idCuota = idCuota;
	}

	public Prestamo getPrestamo() {
		return prestamo;
	}

	public void setPrestamo(Prestamo prestamo) {
		this.prestamo = prestamo;
	}

	public Integer getNumeroDeCuota() {
		return numeroDeCuota;
	}

	public void setNumeroDeCuota(Integer numeroDeCuota) {
		this.numeroDeCuota = numeroDeCuota;
	}

	public LocalDate getFechaDePago() {
		return fechaDePago;
	}

	public void setFechaDePago(LocalDate fechaDePago) {
		this.fechaDePago = fechaDePago;
	}

	public Boolean getEstadoDePago() {
		return estadoDePago;
	}
	
	public void setEstadoDePago(Boolean estadoDePago) {
		this.estadoDePago = estadoDePago;
	}
}
