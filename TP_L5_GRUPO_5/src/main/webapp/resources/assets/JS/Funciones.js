const GetContentPage = () => {
    return $("#content-page");
}

const ClearFields = (idParent) => {
    if (idParent) {
        $(`#${idParent} input[id^=txt]`).val("");
        $(`#${idParent} select[id^=cmb]`).prop("selectedIndex", 0);
        $(`#${idParent} [data-dynamic=1]`).empty();
    }
    else {
        $("input[id^=txt]").val("");
        $("select[id^=cmb]").prop("selectedIndex", 0);
        $("[data-dynamic=1]").empty();
    }
}

const Financial = (x) => {
    return Number.parseFloat(x).toFixed(2);
}

const FillSelect = async (url, target, valueAttr, textAttr) => {
    var result = await fetch(url).then(response => {
        if (response.ok)
            return response.json();
        else
            throw new Error(response.statusText);
    }).catch(console.log);

    $(target).empty();
    result.forEach(el => {
        $(target).append(new Option(el[textAttr], el[valueAttr]));
    });
}

const EventDelete = (idElement, text) => {
    $("#TabContent").on("click", `[id$=${idElement}]`, (event) => {
        event.preventDefault();
        var url = $(event.currentTarget).attr("href");

        swal({
            title: text,
            icon: "warning",
            buttons: true,
            dangerMode: true,
        }).then((borrar) => {
            if (borrar) {
                $.get({
                    url: url,
                    success: (res) => {
                        if (res.message == "OK")
                            swal("Éxito!").then(() => location.reload(true));
                        else
                            ShowWarning(res.details);
                    }
                });
            }
        });
    });
}

const ShowWarning = (lines) => {
    var errors = "";
    if (Array.isArray(lines)) lines.forEach(e => errors += `${e} <br/>`);
    else errors = lines;
    swal({
        title: "Atención",
        content: $("<p>").html(errors)[0],
        icon: "warning"
    });
}

const FilterByColumn = (table, header, value) => {
    var numColumnas = table.columns().data().length;
    var index = -1;
    for (var i = 0; i < numColumnas; ++i) {
        if (table.column(i).header().innerHTML == header) {
            index = i;
            break;
        }
    }
    if (index != -1)
        table.column(index).search(value).draw();
}

const RemoveFilters = (table) => {
    table.search('').columns().search('').draw();
}