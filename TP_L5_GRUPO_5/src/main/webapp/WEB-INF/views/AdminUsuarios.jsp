<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Usuarios</title>
</head>

<body>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="listado-tab" data-toggle="tab" href="#listado" role="tab"
                aria-controls="home" aria-selected="true">Listado</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="detalle-tab" data-toggle="tab" href="#detalle" role="tab" aria-controls="profile"
                aria-selected="false">Detalle</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="formulario-tab" data-toggle="tab" href="#formulario" role="tab"
                aria-controls="profile" aria-selected="false">Usuario <span id="spnState" class="badge badge-secondary">Nuevo</span></a>
        </li>
    </ul>

    <div class="tab-content" id="TabContent">
        <div class="tab-pane fade show active" id="listado" role="tabpanel" aria-labelledby="home-tab">
            <div class="container mt-3">
                <h2>Usuarios en el sistema</h2>
                <hr />

                <div class="row mb-3">
                    <div class="container-fluid">
                        <div class="accordion" id="cont-filtros">
                            <div class="card">
                                <div class="card-header">
                                    <a class="btn btn-link btn-block text-left" data-toggle="collapse"
                                        href="#collapseFiltros" role="button" aria-expanded="false">
                                        Filtros
                                    </a>
                                </div>

                                <div id="collapseFiltros" class="collapse" data-parent="#cont-filtros">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <select id="cmbFiltroEstado" class="form-control">
                                                    <option>Filtrar por Estado</option>
                                                    <option>Activo</option>
                                                    <option>Suspendido</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <button id="btnReiniciarFiltros"
                                                    class="btn btn-secondary">Reiniciar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tblListado">
                    <table id="tblUsuarios">
                        <thead>
                            <tr style="background-color: deepskyblue;">
                                <th>Acciones</th>
                                <th>DNI</th>
                                <th>Usuario</th>
                                <th>Apellido</th>
                                <th>Email</th>
                                <th>Fecha de Nac.</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="usuario" items="${listaUsuarios}">
                                <tr style="background-color: bisque;">
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a id="btnEditar" class="btn btn-success btn-sm" title="Editar"
                                                href="./usuarios/${usuario.getUser()}">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            <a id="btnEliminar" class="btn btn-danger btn-sm" title="Suspender"
                                                href="./usuarios/delete/${usuario.getUser()}">
                                                <i class="fas fa-times"></i>
                                            </a>
                                            <a id="btnDetalle" class="btn btn-info btn-sm" title="Vista detallada"
                                                href="./usuarios/${usuario.getUser()}">
                                                <i class="far fa-file"></i>
                                            </a>
                                            <a id="btnAddCuenta" class="btn btn-primary btn-sm"
                                                title="Asignar nueva cuenta"
                                                href="./cuentas/check?user=${usuario.getDNI()}">
                                                <i class="fas fa-plus"></i>
                                            </a>
                                        </div>
                                    </td>
                                    <td>${usuario.getDNI()}</td>
                                    <td>${usuario.getUser()}</td>
                                    <td>${usuario.getApellido()}</td>
                                    <td>${usuario.getEmail()}</td>
                                    <td>${usuario.getFechaDeNacimiento()}</td>
                                    <c:choose>
                                        <c:when test="${usuario.getEstado()==true}">
                                            <td>Activo</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>Suspendido</td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div class="row justify-content-center mt-3">
                    <input type="button" id="btnNuevo" class="btn btn-primary" value="Agregar nuevo">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="detalle" role="tabpanel" aria-labelledby="profile-tab">
            <div id="lblSinResultados" class="row justify-content-center mt-3">
                <h3 class="font-weight-bold">Sin resultados</h3>
            </div>
            <div id="panelDatos" class="container mt-3" style="display: none;">
                <h2>Detalle del usuario</h2>
                <hr />

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Usuario</span>
                        </div>
                        <input type="text" id="detUser" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Email</span>
                        </div>
                        <input type="text" id="detEmail" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-2 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">DNI</span>
                        </div>
                        <input type="text" id="detDNI" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-10 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Apellido y nombre</span>
                        </div>
                        <input type="text" id="detNombre" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Fecha de nacimiento</span>
                        </div>
                        <input type="text" id="detFechaNac" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Nacionalidad</span>
                        </div>
                        <input type="text" id="detNacionalidad" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Provincia</span>
                        </div>
                        <input type="text" id="detProvincia" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Localidad</span>
                        </div>
                        <input type="text" id="detLocalidad" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Dirección</span>
                        </div>
                        <input type="text" id="detDireccion" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-3 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Sexo</span>
                        </div>
                        <input type="text" id="detSexo" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-3 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Estado</span>
                        </div>
                        <input type="text" id="detEstado" class="form-control" disabled />
                    </div>
                </div>

                <div class="row justify-content-end mb-3 mt-3">
                    <div class="btn-group p-1" role="group">
                        <a id="btnDetEditar" class="btn btn-primary" href="#">Editar</a>
                        <a id="btnDetEliminar" class="btn btn-danger" href="#">Suspender</a>
                    </div>
                </div>

                <div class="row">
                    <div class="container-fluid">
                        <div class="accordion" id="cont-cuentas">
                            <div class="card">
                                <div class="card-header">
                                    <a class="btn btn-link btn-block text-left" data-toggle="collapse"
                                        href="#collapseCuentas" role="button" aria-expanded="false">
                                        Ver cuentas
                                    </a>
                                </div>

                                <div id="collapseCuentas" class="collapse" data-parent="#cont-cuentas">
                                    <div class="card-body">
                                        <!-- En este div se cargan las cuentas -->
                                        <div id="rowCuentas" class="row">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="formulario" role="tabpanel" aria-labelledby="profile-tab">
            <div class="container mt-3">
                <h2>Datos del usuario</h2>
                <hr />

                <form:form id="frmDatos" action="./usuarios/save" modelAttribute="usuario">
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Usuario</label>
                            <form:input path="user" id="txtUser" name="txtUser" pattern=".{1,50}"
                                title="No puede contener más de 50 caracteres" class="form-control"
                                required="required" />
                        </div>

                        <div class="form-group col-6">
                            <label>Contraseña</label>
                            <form:input path="password" type="password" pattern=".{1,40}"
                                title="No puede contener más de 40 caracteres" id="txtPassword" name="txtPassword"
                                class="form-control" required="required" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-4">
                            <label>DNI</label>
                            <form:input path="DNI" type="text" id="txtDNI" name="txtDNI" class="form-control"
                                pattern="[0-9.]{10}" title="El formato debe ser xx.xxx.xxx" required="required" />
                        </div>

                        <div class="form-group col-8">
                            <label>Email</label>
                            <form:input path="email" type="email" id="txtEmail" name="txtEmail"
                                pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{1,50}"
                                title="No tiene el formato de un email o es muy largo" class="form-control"
                                required="required" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Apellido</label>
                            <form:input path="apellido" id="txtApellido" name="txtApellido" pattern=".{1,50}"
                                title="No puede contener más de 50 caracteres" class="form-control"
                                required="required" />
                        </div>

                        <div class="form-group col-6">
                            <label>Nombre</label>
                            <form:input path="nombre" id="txtNombre" name="txtNombre" pattern=".{1,50}"
                                title="No puede contener más de 50 caracteres" class="form-control"
                                required="required" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Fecha de nacimiento</label>
                            <form:input path="fechaDeNacimiento" type="date" id="txtFechaDeNacimiento"
                                name="txtFechaDeNacimiento" class="form-control" required="required" />
                        </div>

                        <div class="form-group col-6">
                            <label>Nacionalidad</label>
                            <form:select path="nacionalidad.id" id="cmbNacionalidad" name="cmbNacionalidad"
                                class="form-control" required="required">
                                <option selected="selected" value="">Seleccione una nacionalidad</option>
                                <c:forEach var="pais" items="${listaPaises}">
                                    <option value="${pais.getId()}">${pais.getNombre()}</option>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Provincia</label>
                            <select id="cmbProvincia" name="cmbProvincia" class="form-control" required="required">
                                <option selected="selected" value="">Seleccione una provincia</option>
                                <c:forEach var="provincia" items="${listaProvincias}">
                                    <option value="${provincia.getIdProvincia()}">${provincia.getNombre()}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <div class="form-group col-6">
                            <label>Localidad</label>
                            <form:select path="localidad.idLocalidad" id="cmbLocalidad" name="cmbLocalidad"
                                class="form-control" required="required" data-dynamic="1">
                                <option value="">Seleccione una localidad</option>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Dirección</label>
                            <form:input path="direccion" id="txtDireccion" name="txtDireccion" pattern=".{1,60}"
                                title="No puede contener más de 60 caracteres" class="form-control"
                                required="required" />
                        </div>

                        <div class="form-group col-3">
                            <label>Perfil</label>
                            <form:select path="perfil" id="cmbPerfil" name="cmbPerfil" class="form-control"
                                required="required">
                                <option value="A">Administrador</option>
                                <option value="C">Cliente</option>
                            </form:select>
                        </div>

                        <div class="form-group col-3">
                            <label>Sexo</label>
                            <div class="form-check">
                                <form:radiobutton path="sexo" class="form-check-input" name="opcSexo" id="opcMasculino"
                                    checked="checked" value="M" required="required" />
                                <label class="form-check-label" for="opcMasculino">Masculino</label>
                            </div>
                            <div class="form-check">
                                <form:radiobutton path="sexo" class="form-check-input" name="opcSexo" id="opcFemenino"
                                    value="F" />
                                <label class="form-check-label" for="opcFemenino">Femenino</label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group">
                            <div class="form-check">
                                <form:checkbox path="estado" id="chkEstado" name="chkEstado" checked="true" />
                                <label class="form-check-label" for="chkEstado">
                                    Activo
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-end">
                        <input type="submit" name="btnAceptar" value="Aceptar" class="btn btn-success pr-3">
                    </div>
                </form:form>
            </div>
        </div>
    </div>

    <input type="hidden" id="hdfUrlSave" name="hdfUrlSave" value="./usuarios/save" />
    <input type="hidden" id="hdfUrlUpdate" name="hdfUrlUpdate" value="./usuarios/update" />

    <script>
        function setMaxDate() {
            var today = new Date();
            var year = today.getFullYear();
            var dif = new Date(`\${year}-1-02`) - new Date(`\${year - 18}-1-01`);
            var maxDate = new Date(today - new Date(dif));
            $("#txtFechaDeNacimiento").attr("max", maxDate.toISOString().split("T")[0]);
        }

        $(document).ready(function () {
            initTable("tblUsuarios", {
                columnDefs: [{
                    targets: 5, render: data => {
                        return moment(data).format('DD/MM/YYYY');
                    }
                }]
            });
            $("#txtDNI").mask("00.000.000");
            setMaxDate();

            EventDelete("btnEliminar", "¿Está seguro que desea dar de baja el usuario?");
            EventDelete("btnDetEliminar", "¿Está seguro que desea dar de baja el usuario?");

            $("#collapseFiltros").on("change", "#cmbFiltroEstado", (e) => {
                var valor = $("option:selected", $(e.currentTarget)).val();
                FilterByColumn($("#tblUsuarios").DataTable(), "Estado", valor);
            });

            $("#collapseFiltros").on("click", "#btnReiniciarFiltros", () => {
                ClearFields("cont-filtros");
                RemoveFilters($("#tblUsuarios").DataTable());
            });

            $("#btnNuevo").on("click", () => {
                $("#spnState").html("Nuevo");
                $('#formulario-tab').tab('show');
                $("#frmDatos").attr("action", $("#hdfUrlSave").val());
                ClearFields();
                $("[id$=txtUser]").attr("readonly", false);
            });

            $("#frmDatos").on("submit", function (event) {
                event.preventDefault();
                $.post({
                    url: $(this).attr("action"),
                    data: $(this).serialize(),
                    success: function (res) {
                        if (res.message == "OK") {
                            swal("Éxito!").then(() => location.reload(true));
                        }
                        else {
                            ShowWarning(res.details);
                        }
                    }
                });
            });

            $("#TabContent").on("click", "#btnEditar, #btnDetEditar", function (event) {
                event.preventDefault();
                $.get({
                    url: $(this).attr("href"),
                    success: async function (res) {
                        $("#spnState").html("Edit");
                        $('#formulario-tab').tab('show');
                        $("#frmDatos").attr("action", $("#hdfUrlUpdate").val());
                        $("[id$=txtPassword]").attr("required", false);
                        ClearFields();

                        if (res.message == "OK") {
                            var usuario = res.data;
                            $("[id$=txtUser]").val(usuario.user);
                            $("[id$=txtUser]").attr("readonly", true);
                            $("[id$=txtDNI]").val(usuario.dni);
                            $("[id$=txtEmail]").val(usuario.email);
                            $("[id$=txtNombre]").val(usuario.nombre);
                            $("[id$=txtApellido]").val(usuario.apellido);

                            var fechaNac = usuario.fechaDeNacimiento;
                            var fechaValue = new Date(fechaNac.year, fechaNac.monthValue - 1, fechaNac.dayOfMonth);
                            $("[id$=txtFechaDeNacimiento]").val(fechaValue.toISOString().split("T")[0]);
                            $("[id$=cmbNacionalidad]").val(usuario.nacionalidad.id);

                            $("[id$=cmbProvincia]").val(usuario.localidad.provincia.idProvincia);
                            var url = `./resources/localidades?idProvincia=\${usuario.localidad.provincia.idProvincia}`;
                            await FillSelect(url, $("#cmbLocalidad"), "idLocalidad", "nombre");

                            $("[id$=cmbLocalidad]").val(usuario.localidad.idLocalidad);
                            $("[id$=txtDireccion]").val(usuario.direccion);
                            $("[id$=cmbPerfil]").val(usuario.perfil);

                            var sexo = (usuario.sexo == "M") ? "optMasculino" : "optFemenino";
                            $("#" + sexo).prop("checked", true);

                            $("#chkEstado").attr("checked", usuario.estado);
                        }
                        else {
                            ShowWarning(res.message);
                        }
                    }
                });
            });

            $("#TabContent").on("click", "#btnDetalle", function (event) {
                event.preventDefault();
                $("#collapseCuentas").removeClass("show");
                $("#rowCuentas").empty();

                $.get({
                    url: $(this).attr("href"),
                    success: function (res) {
                        if (res.message == "OK") {
                            $('#detalle-tab').tab('show');
                            $("#lblSinResultados").hide();
                            $("#panelDatos").show();

                            var usuario = res.data;
                            $("[id$=detUser]").val(usuario.user);
                            $("[id$=detDNI]").val(usuario.dni);
                            $("[id$=detEmail]").val(usuario.email);
                            $("[id$=detNombre]").val(usuario.apellido + " " + usuario.nombre);

                            var fechaNac = usuario.fechaDeNacimiento;
                            $("[id$=detFechaNac]").val(`\${fechaNac.dayOfMonth}/\${fechaNac.monthValue}/\${fechaNac.year}`);
                            $("[id$=detNacionalidad]").val(usuario.nacionalidad.nombre);
                            $("[id$=detProvincia]").val(usuario.localidad.provincia.nombre);
                            $("[id$=detLocalidad]").val(usuario.localidad.nombre);
                            $("[id$=detDireccion]").val(usuario.direccion);

                            var sexo = (usuario.sexo == "M") ? "Masculino" : "Femenino";
                            $("[id$=detSexo]").val(sexo);
                            var estado = (usuario.estado) ? "Activo" : "Suspendido";
                            $("[id$=detEstado]").val(estado);

                            $("[id$=btnDetEditar]").attr("href", "./usuarios/" + usuario.user);
                            $("[id$=btnDetEliminar]").attr("href", "./usuarios/delete/" + usuario.user);

                            // Carga el colapsable de cuentas
                            usuario.cuentas.forEach(cuenta => {
                                var borderClass = (cuenta.estado) ? "border-info" : "border-danger";
                                var bodyClass = (cuenta.estado) ? "text-info" : "text-danger";

                                var col = $("<div>").addClass("col-sm-3");
                                var card = $("<div>").addClass(`card \${borderClass}`).css("min-height", "100%");

                                // Header del Card
                                var cardHeader = $("<div>").addClass("card-header");
                                var cardTitle = $("<h5>").addClass("card-title").html(`Alias: \${cuenta.alias}`);
                                cardHeader.append(cardTitle);

                                // Body del Card
                                var cardBody = $("<div>").addClass(`card-body \${bodyClass}`);
                                var row1 = $("<div>").addClass("row mb-1");
                                row1.append($("<div>").addClass("col-sm-5").append(
                                    $("<span>").addClass("font-weight-bold").html("Tipo:")
                                )).append($("<div>").addClass("col-sm-7").append(
                                    $("<label>").html(`\${cuenta.tipoDeCuenta.descripcion}`)
                                ));

                                var row2 = $("<div>").addClass("row mb-1");
                                row2.append($("<div>").addClass("col-sm-5").append(
                                    $("<span>").addClass("font-weight-bold").html("Saldo:")
                                )).append($("<div>").addClass("col-sm-7").append(
                                    $("<label>").html(`$ \${cuenta.saldo}`)
                                ));

                                cardBody.append(row1);
                                cardBody.append(row2);

                                // Footer del Card
                                var cardFooter = $("<div>").addClass("card-footer");
                                var btnAdmin = $("<a>").addClass("btn btn-primary").html("Administrar").attr("href", `./cuentas/edit?alias=\${cuenta.alias}`).attr("id", "btnEditCuenta");
                                cardFooter.append(btnAdmin);


                                card.append(cardHeader);
                                card.append(cardBody);
                                card.append(cardFooter);
                                col.append(card);
                                $("#rowCuentas").append(col);
                            });
                        }
                        else {
                            ShowWarning(res.message);
                        }
                    }
                });
            });

            $("#tblUsuarios").on("click", "#btnAddCuenta", function (event) {
                event.preventDefault();
                var ruta = $(this).attr("href");

                $.get({
                    url: ruta,
                    success: function (res) {
                        if (res.message != "OK") {
                            swal("El usuario ya alcanzó el límite de cuentas");
                        }
                        else {
                            var user = ruta.split("=").pop();
                            /// Redirigir al ABM de cuentas
                            $("#content-page").load(`./cuentas/add?user=\${user}`);
                        }
                    }
                });
            });

            $("#TabContent").on("click", "#btnEditCuenta", function (event) {
                event.preventDefault();
                $("#content-page").load($(this).attr("href"));
            });

            $("#frmDatos").on("change", "#cmbProvincia", () => {
                var idProvincia = $('option:selected', $("#cmbProvincia")).val();
                FillSelect(`./resources/localidades?idProvincia=\${idProvincia}`, $("#cmbLocalidad"), "idLocalidad", "nombre");
            });
        });
    </script>

</body>

</html>