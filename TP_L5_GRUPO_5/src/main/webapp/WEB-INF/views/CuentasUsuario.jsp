<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Cuentas</title>
</head>

<body>

    <div class="container mt-3">
        <c:choose>
            <c:when test="${message != null}">
                <h2>Aún no tenés cuentas disponibles.</h2>
            </c:when>
            <c:otherwise>
                <h2>Estas son tus cuentas</h2>
                <hr />

                <div id="rowCuentas" class="row mt-3">
                    <c:forEach var="c" items="${listaCuentas}">
                        <div class="col-sm-3">
                            <div class="card border-info" style="min-height: 100%;">
                                <div class="card-header">
                                    <h5 class="card-title">Alias: ${c.getAlias()}</h5>
                                    <h6 class="card-title"><a id="btnVerMov" class="text-muted"
                                            href="./movimientos?alias=${c.getAlias()}">Ver movimientos</a></h6>
                                </div>
                                <div class="card-body text-info">
                                    <div class="row mb-1">
                                        <div class="col-sm-5"><span class="font-weight-bold">Tipo:</span></div>
                                        <div class="col-sm-7">
                                            <label>${c.getTipoDeCuenta().getDescripcion()}</label>
                                        </div>
                                    </div>
                                    <div class="row mb-1">
                                        <div class="col-sm-5"><span class="font-weight-bold">Saldo:</span></div>
                                        <div class="col-sm-7"><label>$${c.getSaldo()}</label></div>
                                    </div>
                                    <c:choose>
                                        <c:when test="${c.getEsPrincipal()==true}">
                                            <div class="row justify-content-center mb-1">
                                                <strong>Cuenta principal</strong>
                                            </div>
                                        </c:when>
                                    </c:choose>
                                </div>
                                <div class="card-footer"><a class="btn btn-primary"
                                        href="./cuentas/setprincipal?alias=${c.getAlias()}" id="btnSetMain">Seleccionar
                                        como
                                        principal</a></div>
                            </div>
                        </div>
                    </c:forEach>
                </div>
            </c:otherwise>
        </c:choose>
    </div>

    <script>
        $(() => {
            $("#rowCuentas").on("click", "[id=btnSetMain]", (e) => {
                e.preventDefault();
                $.get({
                    url: $(e.target).attr("href"),
                    success: (res) => {
                        if (res.message == "OK") {
                            swal("Éxito!").then(() => location.reload(true));
                        }
                        else {
                            ShowWarning(res.details);
                        }
                    }
                });
            });

            $("#rowCuentas").on("click", "[id=btnVerMov]", (e) => {
                e.preventDefault();
                $("#content-page").load($(e.target).attr("href"));
            });
        })
    </script>

</body>

</html>