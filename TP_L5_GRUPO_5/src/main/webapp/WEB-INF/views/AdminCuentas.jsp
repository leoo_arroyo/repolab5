<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Cuentas</title>
</head>

<body>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="listado-tab" data-toggle="tab" href="#listado" role="tab"
                aria-controls="home" aria-selected="true">Listado</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="formulario-tab" data-toggle="tab" href="#formulario" role="tab"
                aria-controls="profile" aria-selected="false">Cuenta <span id="spnState" class="badge badge-secondary">Nuevo</span></a>
        </li>
    </ul>

    <div class="tab-content" id="TabContent">
        <div class="tab-pane fade show active" id="listado" role="tabpanel" aria-labelledby="home-tab">
            <div class="container mt-3">
                <h2>Cuentas en el sistema</h2>
                <hr />

                <div class="row mb-3">
                    <div class="container-fluid">
                        <div class="accordion" id="cont-filtros">
                            <div class="card">
                                <div class="card-header">
                                    <a class="btn btn-link btn-block text-left" data-toggle="collapse"
                                        href="#collapseFiltros" role="button" aria-expanded="false">
                                        Filtros
                                    </a>
                                </div>

                                <div id="collapseFiltros" class="collapse" data-parent="#cont-filtros">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <select id="cmbFiltroTipo" class="form-control">
                                                    <option>Filtrar por Tipo de cuenta</option>
                                                    <c:forEach var="tipo" items="${listaTiposCuenta}">
                                                        <option value="${tipo.getId()}">${tipo.getDescripcion()}</option>
                                                    </c:forEach>
                                                </select>
                                            </div>

                                            <div class="col-sm-4">
                                                <select id="cmbFiltroEstado" class="form-control">
                                                    <option>Filtrar por Estado</option>
                                                    <option>Activo</option>
                                                    <option>Suspendido</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-2">
                                                <button id="btnReiniciarFiltros"
                                                    class="btn btn-secondary">Reiniciar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="tblListado">
                    <table id="tblCuentas">
                        <thead>
                            <tr style="background-color: deepskyblue;">
                                <th>Acciones</th>
                                <th>CBU</th>
                                <th>Número de cuenta</th>
                                <th>Fecha de creación</th>
                                <th>Tipo de cuenta</th>
                                <th>Alias</th>
                                <th>Saldo</th>
                                <th>Usuario</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="cuenta" items="${listaCuentas}">
                                <tr style="background-color: bisque;">
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a id="btnEditar" class="btn btn-success btn-sm" title="Editar"
                                                href="./cuentas/${cuenta.getCBU()}">
                                                <i class="fas fa-pencil-alt"></i>
                                            </a>
                                            <a id="btnEliminar" class="btn btn-danger btn-sm" title="Suspender"
                                                href="./cuentas/delete/${cuenta.getCBU()}">
                                                <i class="fas fa-times"></i>
                                            </a>
                                        </div>
                                    </td>
                                    <td>${cuenta.getCBU()}</td>
                                    <td>${cuenta.getNumeroDeCuenta()}</td>
                                    <td>${cuenta.getFechaCreacion()}</td>
                                    <td>${cuenta.getTipoDeCuenta().getId()} - ${cuenta.getTipoDeCuenta().getDescripcion()}</td>
                                    <td>${cuenta.getAlias()}</td>
                                    <td>$ ${cuenta.getSaldo()}</td>
                                    <td>${cuenta.getUsuario().getUser()}</td>
                                    <c:choose>
                                        <c:when test="${cuenta.getEstado()==true}">
                                            <td>Activo</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>Suspendido</td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div class="row justify-content-center mt-3">
                    <input type="button" id="btnNuevo" class="btn btn-primary" value="Agregar nuevo">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="formulario" role="tabpanel" aria-labelledby="profile-tab">
            <div class="container mt-3">
                <h2>Datos de la cuenta</h2>
                <hr />

                <form:form id="frmDatos" action="./cuentas/save" modelAttribute="cuenta">
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Usuario</label>
                            <div>
                                <form:input path="usuario.DNI" type="text" id="txtUserDNI" name="txtUserDNI" value="${user}"
                                    class="form-control" data-toggle="dropdown" autocomplete="off"
                                    placeholder="Buscar por nombre de usuario..." pattern=".{1,50}"
                                    title="No debe contener más de 50 caracteres" required="required" />
                                <div id="options" class="dropdown-menu"
                                    style="min-width: 100%; max-height:300px; overflow-y:auto">
                                    <label class="dropdown-header">La busqueda comienza a partir del segundo
                                        caracter</label>
                                </div>
                            </div>
                        </div>

                        <div class="form-group col-6">
                            <label>Tipo de cuenta</label>
                            <form:select path="tipoDeCuenta.id" id="cmbTipoCuenta" name="cmbTipoCuenta"
                                class="form-control" required="required">
                                <option selected value="">Seleccione un tipo de cuenta</option>
                                <c:forEach var="tipo" items="${listaTiposCuenta}">
                                    <c:choose>
                                        <c:when test="${tipo.getId()==cuentaEdit.getTipoDeCuenta().getId()}">
                                            <option selected value="${tipo.getId()}">${tipo.getDescripcion()}
                                            </option>
                                        </c:when>
                                        <c:otherwise>
                                            <option value="${tipo.getId()}">${tipo.getDescripcion()}</option>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                            </form:select>
                        </div>
                    </div>

                    <div class="form-row col-6 mb-3">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="opcBusqueda" id="opcNombre"
                                checked="checked" value="byNombre" required="required">
                            <label class="form-check-label" for="opcNombre">Nombre de usuario</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" type="radio" name="opcBusqueda" id="opcDNI"
                                value="byDNI">
                            <label class="form-check-label" for="opcDNI">DNI</label>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>CBU</label>
                            <form:input path="CBU" type="text" id="txtCBU" name="txtCBU" class="form-control" min="1"
                                value="${cuentaEdit.getCBU()}" pattern="[0-9 ]{26}"
                                title="El formato debe ser xxx xxxx x xxxxxxxxxx x" required="required" />
                        </div>

                        <div class="form-group col-6">
                            <label>Número de cuenta</label>
                            <form:input path="numeroDeCuenta" type="text" id="txtNumeroDeCuenta"
                                name="txtNumeroDeCuenta" class="form-control" value="${cuentaEdit.getNumeroDeCuenta()}"
                                pattern="[0-9 ]{23}" title="El formato debe ser xxxx xxxx xx xxxxxxxxxx"
                                required="required" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Alias</label>
                            <form:input path="alias" id="txtAlias" name="txtAlias" class="form-control"
                                pattern=".{1,50}" title="No puede contener más de 50 caracteres"
                                value="${cuentaEdit.getAlias()}" required="required" />
                        </div>

                        <div class="form-group col-6">
                            <label>Saldo</label>
                            <form:input path="saldo" type="text" id="txtSaldo" name="txtSaldo" class="form-control"
                                value="${cuentaEdit.getSaldo() == null? 10000 : cuentaEdit.getSaldo()}" required="required" />
                        </div>
                    </div>

                    <div class="form-row justify-content-center">
                        <div class="form-group">
                            <div class="form-check">
                                <c:choose>
                                    <c:when test="${cuentaEdit.getEstado()==false}">
                                        <form:checkbox path="estado" id="chkEstado" name="chkEstado" />
                                    </c:when>
                                    <c:otherwise>
                                        <form:checkbox path="estado" id="chkEstado" name="chkEstado" checked="true" />
                                    </c:otherwise>
                                </c:choose>
                                <label class="form-check-label" for="chkEstado">
                                    Activo
                                </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-row justify-content-end">
                        <input type="submit" name="btnAceptar" value="Aceptar" class="btn btn-success pr-3">
                    </div>
                </form:form>
            </div>
        </div>
    </div>

    <input type="hidden" id="hdfUrlSave" name="hdfUrlSave" value="./cuentas/save" />
    <input type="hidden" id="hdfUrlUpdate" name="hdfUrlUpdate" value="./cuentas/update" />

    <script>
        $(document).ready(function () {
            initTable("tblCuentas", {
                columnDefs: [{
                    targets: 3, render: data => {
                        return moment(data).format('DD/MM/YYYY');
                    }
                }]
            });

            $("#txtCBU").mask("000 0000 0 0000000000000 0");
            $("#txtNumeroDeCuenta").mask("0000 0000 00 0000000000");
            $("#txtSaldo").attr("readonly", true);

            EventDelete("btnEliminar", "¿Está seguro que desea dar de baja la cuenta?");

            $("#collapseFiltros").on("change", "#cmbFiltroTipo", (e) => {
                var valor = $("option:selected", $(e.currentTarget)).val();
                FilterByColumn($("#tblCuentas").DataTable(), "Tipo de cuenta", valor);
            });

            $("#collapseFiltros").on("change", "#cmbFiltroEstado", (e) => {
                var valor = $("option:selected", $(e.currentTarget)).val();
                FilterByColumn($("#tblCuentas").DataTable(), "Estado", valor);
            });

            $("#collapseFiltros").on("click", "#btnReiniciarFiltros", () => {
                ClearFields("cont-filtros");
                RemoveFilters($("#tblCuentas").DataTable());
            });

            // Si al cargar la pagina ya tiene un usuario, significa que viene redirigido
            // del ABM de usuarios
            if ($("#txtUserDNI").val()) {
                $('#formulario-tab').tab('show');
                // Se evalua el CBU para saber si es una nueva carga de cuenta o se edita una ya existente
                var esNueva = ($("#txtCBU").val() == "");

                var action = esNueva ? $("#hdfUrlSave").val() : $("#hdfUrlUpdate").val();
                $("#frmDatos").attr("action", action);
                $("[id$=txtCBU]").attr("readonly", !(esNueva));
                (esNueva) ? $("#txtSaldo").val("10000").attr("readonly", true) : $("#txtSaldo").attr("readonly", false);
            }

            $("#btnNuevo").on("click", () => {
                $("#spnState").html("Nuevo");
                $('#formulario-tab').tab('show');
                $("#frmDatos").attr("action", $("#hdfUrlSave").val());
                ClearFields();
                $("[id$=txtCBU]").attr("readonly", false);
                $("#txtSaldo").val("10000").attr("readonly", true);
            });

            $("#frmDatos").on("submit", function (event) {
                event.preventDefault();
                $.post({
                    url: $(this).attr("action"),
                    data: $(this).serialize(),
                    success: function (res) {
                        if (res.message == "OK") {
                            swal("Éxito!").then(() => location.reload(true));
                        }
                        else {
                            ShowWarning(res.details);
                        }
                    }
                });
            });

            $("#TabContent").on("click", "#btnEditar", function (event) {
                event.preventDefault();
                $.get({
                    url: $(this).attr("href"),
                    success: function (res) {
                        $("#spnState").html("Edit");
                        $('#formulario-tab').tab('show');
                        $("#frmDatos").attr("action", $("#hdfUrlUpdate").val());
                        ClearFields();

                        var cuenta = res.data;
                        $("[id$=txtUserDNI]").val(cuenta.usuario.dni);
                        $("[id$=cmbTipoCuenta]").val(cuenta.tipoDeCuenta.id);
                        $("[id$=txtCBU]").val(cuenta.cbu).attr("readonly", true);
                        $("[id$=txtNumeroDeCuenta]").val(cuenta.numeroDeCuenta);
                        $("[id$=txtAlias]").val(cuenta.alias);
                        $("[id$=txtSaldo]").val(cuenta.saldo).attr("readonly", false);
                        $("#chkEstado").attr("checked", cuenta.estado);
                    }
                });
            });

            $("#txtUserDNI").on("keyup", async (e) => {
                e.target.setAttribute("data-selected-value", "-1");
                var valor = e.target.value;
                if (valor.length >= 2) {
                    var type = $("[name=opcBusqueda]:checked").val();
                    var users = await fetch(`./usuarios/filter?search=\${valor}&type=\${type}`).then(res => {
                        if (res.ok)
                            return res.json();
                        else
                            throw new Error(res.statusText);
                        return false;
                    }).catch(console.log);

                    $("[id$=options]").empty();

                    var map = users.data;
                    if (map) {
                        $.each(map, (key, value) => {
                            $("[id$=options]").append($("<label>").text(value).attr("data-value", key).addClass("dropdown-item"));
                        });
                    }
                    else
                        $("[id$=options]").append($("<label>").text("Sin resultados").addClass("dropdown-header"));
                }
                else {
                    $("[id$=options]").empty();
                    $("[id$=options]").append($("<label>").text("La busqueda comienza a partir del segundo caracter").addClass("dropdown-header"));
                }
            });

            $("#options").on("click", ".dropdown-item", function (event) {
                event.preventDefault();
                $("#txtUserDNI").val($(this).attr("data-value"));
            });

            $("[name=opcBusqueda]").on("change", (e) => {
                switch (e.target.value) {
                    case "byNombre":
                        $("#txtUserDNI").attr("placeholder", "Buscar por nombre de usuario...");
                        break;
                    case "byDNI":
                        $("#txtUserDNI").attr("placeholder", "Buscar por DNI...");
                        break;
                }
            });
        });

    </script>

</body>

</html>