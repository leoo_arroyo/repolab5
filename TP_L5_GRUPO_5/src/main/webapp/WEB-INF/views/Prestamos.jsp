<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Préstamos</title>
</head>

<body>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="listado-tab" data-toggle="tab" href="#listado" role="tab"
                aria-controls="home" aria-selected="true">Listado</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="formulario-tab" data-toggle="tab" href="#formulario" role="tab"
                aria-controls="profile" aria-selected="false">Solicitar préstamo</a>
        </li>
    </ul>

    <div class="tab-content" id="TabContent">
        <div class="tab-pane fade show active" id="listado" role="tabpanel" aria-labelledby="home-tab">
            <div class="container mt-3">
                <h2>Préstamos</h2>
                <hr />

                <div id="tblListado">
                    <table id="tblPrestamos">
                        <thead>
                            <tr style="background-color: deepskyblue;">
                                <th>Acciones</th>
                                <th>Fecha de solicitud</th>
                                <th>Fecha de aprobación</th>
                                <th>Importe</th>
                                <th>Plazo</th>
                                <th>Interés</th>
                                <th>Importe p/ cuota</th>
                                <th>CBU cuenta asignada</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="prs" items="${listaPrestamos}">
                                <tr style="background-color: bisque;">
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <a id="btnVerCuotas" class="btn btn-success btn-sm" title="Ver cuotas"
                                                href="./prestamos/cuotas?idPrestamo=${prs.getIdPrestamo()}">
                                                Ver cuotas
                                            </a>
                                        </div>
                                    </td>
                                    <td>${prs.getFechaSolicitud()}</td>
                                    <td>${prs.getFechaAprobacion()}</td>
                                    <td>$ ${prs.getImporte()}</td>
                                    <td>${prs.getPlazo()}</td>
                                    <td>${prs.getInteres()} %</td>
                                    <td>$ ${prs.getImportePorCuota()}</td>
                                    <td>${prs.getCuenta().getCBU()}</td>
                                    <td>${prs.getEstado().getDescripcion()}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div class="row justify-content-center">
                    <input type="button" id="btnNuevo" class="btn btn-primary" value="Solicitar préstamo">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="formulario" role="tabpanel" aria-labelledby="profile-tab">
            <div class="container mt-3">
                <h2>Solicitar préstamo</h2>
                <hr />

                <form:form id="frmDatos" action="./prestamos/save" modelAttribute="prestamo">
                    <div class="form-row">
                        <div class="form-group col-6">
                            <label>Importe solicitado</label>
                            <form:input path="importe" type="number" id="txtImporte" name="txtImporte"
                                class="form-control" min="0" required="required" />
                        </div>

                        <div class="form-group col-6">
                            <label>Selección de cuenta</label>
                            <form:select path="cuenta.CBU" id="cmbCuenta" name="cmbCuenta" class="form-control"
                                required="required">
                                <c:forEach var="c" items="${listaCuentas}">
                                    <option value="${c.getCBU()}">${c.getAlias()}</option>
                                </c:forEach>
                            </form:select>
                            <div><label class="text-muted">En esta cuenta recibiras el dinero si se aprueba el
                                    préstamo</label></div>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-4">
                            <label>Plazo</label>
                            <form:select path="plazo" id="cmbPlazo" name="cmbPlazo" class="form-control"
                                required="required">
                                <option value="">Seleccione un plazo</option>
                                <c:forEach var="i" begin="1" end="12">
                                    <option value="${i}">${i} pagos</option>
                                </c:forEach>
                            </form:select>
                        </div>

                        <div class="form-group col-4">
                            <label>Interés</label>
                            <input type="text" id="txtInteres" name="txtInteres" class="form-control"
                                required="required" />
                        </div>

                        <div class="form-group col-4">
                            <label>Importe por cuota</label>
                            <input id="txtImportePorCuota" name="txtImportePorCuota" class="form-control"
                                required="required" />
                        </div>
                    </div>

                    <div class="form-row justify-content-end">
                        <input type="submit" name="btnAceptar" value="Aceptar" class="btn btn-success pr-3">
                    </div>
                </form:form>
            </div>
        </div>
    </div>

    <script>
        $(() => {
            initTable("tblPrestamos", {
                orderFixed: [ 1, 'desc' ],
                columnDefs: [{
                    targets: [1, 2], render: data => (data) ? moment(data).format('DD/MM/YYYY') : "-"
                }]
            });

            $("#txtInteres").attr("readonly", true);
            $("#txtImportePorCuota").attr("readonly", true);

            $("#btnNuevo").on("click", () => {
                $('#formulario-tab').tab('show');
                ClearFields();
            });

            $("#frmDatos").on("submit", (event) => {
                event.preventDefault();
                $.post({
                    url: $(event.target).attr("action"),
                    data: $(event.target).serialize(),
                    success: res => {
                        if (res.message == "OK") {
                            swal("Éxito!").then(() => location.reload(true));
                        }
                        else {
                            ShowWarning(res.details);
                        }
                    }
                });
            });

            $("#tblListado").on("click", "#btnVerCuotas", (e) => {
                e.preventDefault();
                GetContentPage().load($(e.currentTarget).attr("href"));
            });

            $("#cmbPlazo").on("change", (e) => {
                const intMinimo = 30;
                const intMaximo = 50;

                var plazo = $(e.target).val();
                if (plazo == "") {
                    $("#txtInteres").val("");
                    $("#txtImportePorCuota").val("");
                }
                else {
                    var interes = Financial(intMinimo + (((intMaximo - intMinimo) / 11) * (plazo - 1)));
                    $("#txtInteres").val(interes + "%");
                    $("#txtImportePorCuota").val(Financial(($("#txtImporte").val() / plazo) * ((interes / 100) + 1)));
                }
            })
        });
    </script>

</body>

</html>