<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
</head>

<body>
	<div class="tab-content" id="TabContent">
		<div class="tab-pane fade show active" id="listado" role="tabpanel" aria-labelledby="home-tab">
			<div class="container mt-3">
				<h1>Movimientos</h1>
				<hr>
				<div class="row mb-3">
					<div class="container-fluid">
						<div class="accordion" id="cont-filtros">
							<div class="card">
								<div class="card-header">
									<a class="btn btn-link btn-block text-left" data-toggle="collapse"
										href="#collapseFiltros" role="button" aria-expanded="false"> Filtros </a>
								</div>

								<div id="collapseFiltros" class="collapse" data-parent="#cont-filtros">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-4">
												<select id="cmbFiltroTipo" class="form-control">
													<option>Filtrar por Tipo</option>
													<c:forEach var="t" items="${listaTiposMov}">
														<option>${t.getDescripcion()}</option>
													</c:forEach>
												</select>
											</div>
											<div class="col-sm-2">
												<button id="btnReiniciarFiltros"
													class="btn btn-secondary">Reiniciar</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<table id="tbLMovimientos">
						<thead>
							<tr style="background-color: deepskyblue;">
								<th>CBU</th>
								<th>Fecha</th>
								<th>Importe</th>
								<th>Detalle</th>
								<th>Tipo de movimiento</th>
							</tr>
						</thead>
						<c:forEach var="movimiento" items="${listaMovimientos}">
							<tr style="background-color: bisque;">
								<td>${movimiento.getCuenta().getCBU()}</td>
								<td>${movimiento.getFecha()}</td>
								<td>$ ${movimiento.getImporte()}</td>
								<td>${movimiento.getDetalle()}</td>
								<td>${movimiento.getTipoMovimiento().getDescripcion()}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function () {
			initTable("tbLMovimientos", {
				order: [[ 1, 'desc' ]],
				orderCellsTop: true,
				fixedHeader: true,
				columnDefs: [{
					targets: 1, render: data => {
						return moment(data).format('DD/MM/YYYY HH:mm');
					}
				}]
			});

			// Setup - add a text input to each footer cell
			$('#tbLMovimientos thead tr').clone(true).appendTo('#tbLMovimientos thead');
			$('#tbLMovimientos thead tr:eq(1) th').each(function (i) {
				var title = $(this).text();
				$(this).html('<input type="text" placeholder="Buscar ' + title + '"/>');
				let table = $("#tbLMovimientos").DataTable();

				$('input', this).on('keyup change', function () {
					if (table.column(i).search() !== this.value) {
						table
							.column(i)
							.search(this.value)
							.draw();
					}
				});
			});

			$("#collapseFiltros").on("change", "#cmbFiltroTipo", (e) => {
				var valor = $("option:selected", $(e.currentTarget)).val();
				FilterByColumn($("#tbLMovimientos").DataTable(), "Tipo de movimiento", valor);
			});

			$("#collapseFiltros").on("click", "#btnReiniciarFiltros", () => {
				ClearFields("cont-filtros");
				RemoveFilters($("#tbLMovimientos").DataTable());
			});
		});
	</script>
</body>

</html>