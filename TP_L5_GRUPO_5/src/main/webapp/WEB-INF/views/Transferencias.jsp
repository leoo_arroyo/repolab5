<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Transferencias</title>
</head>

<body>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="listado-tab" data-toggle="tab" href="#listado" role="tab"
                aria-controls="home" aria-selected="true">Listado</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link disabled" id="formulario-tab" data-toggle="tab" href="#formulario" role="tab"
                aria-controls="profile" aria-selected="false">Nueva transferencia</a>
        </li>
    </ul>

    <div class="tab-content" id="TabContent">
        <div class="tab-pane fade show active" id="listado" role="tabpanel" aria-labelledby="home-tab">
            <div class="container mt-3">
                <h2>Transferencias</h2>
                <hr />

                <div id="tblListado">
                    <table id="tblTransferencias">
                        <thead>
                            <tr style="background-color: deepskyblue;">
                                <th>Fecha</th>
                                <th>CBU Origen</th>
                                <th>CBU Destino</th>
                                <th>Monto transferencia</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="trs" items="${listaTransferencias}">
                                <tr style="background-color: bisque;">
                                    <td>${trs.getFecha()}</td>
                                    <td>${trs.getCuentaOrigen().getCBU()}</td>
                                    <td>${trs.getCuentaDestino().getCBU()}</td>
                                    <c:choose>
                                        <c:when test="${trs.getEstado().getId()==1}">
                                            <td>$ ${trs.getMonto()}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>$ ${trs.getMonto()} (Rechazado)</td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>

                <div class="row justify-content-center mt-3 mb-3">
                    <strong>Nueva transferencia</strong>
                </div>
                <div class="row justify-content-center">
                    <input type="button" id="btnNuevo" data-attr="propia" class="btn btn-primary mr-3"
                        value="A cuenta propia">
                    <input type="button" id="btnNuevo" data-attr="terceros" class="btn btn-primary"
                        value="A cuentas de terceros">
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="formulario" role="tabpanel" aria-labelledby="profile-tab">
            <div class="container mt-3">
                <h2>Nueva transferencia</h2>
                <hr />

                <form id="frmDatos" action="./transferencias/save">
                    <input type="hidden" id="hdfTipoTrs" name="hdfTipoTrs" />
                    <div class="form-row">
                        <h4>Cuenta de origen</h4>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-12">
                            <select id="cmbCuentaOrigen" name="cmbCuentaOrigen" class="form-control"
                                required="required">
                                <option value="-1">Seleccione una cuenta</option>
                                <c:forEach var="c" items="${listaCuentas}">
                                    <option value="${c.getCBU()}">${c.getAlias()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <h4>Cuenta de destino</h4>
                    </div>

                    <div class="form-row d-none" id="rowCuentaPropia">
                        <div class="form-group col-sm-12">
                            <select id="cmbCuentaDestino" name="cmbCuentaDestino" class="form-control"
                                required="required">
                                <option value="-1">Seleccione una cuenta</option>
                                <c:forEach var="c" items="${listaCuentas}">
                                    <option value="${c.getCBU()}">${c.getAlias()}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <div class="form-row d-none" id="rowCuentaTercero">
                        <div class="form-check form-check-inline col-sm-5">
                            <input class="form-check-input" type="radio" name="opcBusqueda" id="opcCBU"
                                checked="checked" value="byCBU" required="required">
                            <input id="txtCBUDestino" name="txtCBUDestino" class="form-control"
                                placeholder="Ingresar CBU" />
                        </div>
                        <div class="form-check form-check-inline col-sm-5">
                            <input class="form-check-input" type="radio" name="opcBusqueda" id="opcAlias"
                                value="byAlias">
                            <input id="txtAliasDestino" name="txtAliasDestino" class="form-control"
                                placeholder="Ingresar Alias" />
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-sm-12">
                            <label>Monto</label>
                            <input id="txtMonto" name="txtMonto" class="form-control" required="required" />
                        </div>
                    </div>

                    <div class="row justify-content-end mt-3">
                        <div class="btn-group p-1" role="group">
                            <a id="btnCancelar" class="btn btn-danger"
                                href="javascript:location.reload(true);">Cancelar</a>
                            <input type="submit" name="btnAceptar" value="Aceptar" class="btn btn-success">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script>
        $(() => {
            initTable("tblTransferencias", {
                order: [[ 0, 'desc' ]],
                columnDefs: [{
                    targets: 0, render: data => {
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                }]
            });

            $("[id=btnNuevo]").on("click", (e) => {
                const tipoTrs = $(e.target).attr("data-attr");
                $("#hdfTipoTrs").val(tipoTrs);
                if (tipoTrs == "propia") {
                    $("#rowCuentaPropia").removeClass("d-none");
                    $("#rowCuentaTercero").addClass("d-none");
                }
                else {
                    $("#rowCuentaTercero").removeClass("d-none");
                    $("#rowCuentaPropia").addClass("d-none");
                }

                $("#formulario-tab").removeClass("disabled");
                $('#formulario-tab').tab('show');
                $("#listado-tab").addClass("disabled");
            });

            $("#frmDatos").on("submit", (event) => {
                event.preventDefault();
                $.post({
                    url: $(event.target).attr("action"),
                    data: $(event.target).serialize(),
                    success: res => {
                        if (res.message == "OK") {
                            swal("Éxito!").then(() => location.reload(true));
                        }
                        else {
                            ShowWarning(res.details);
                        }
                    }
                });
            });

            $("#cmbCuentaOrigen").on("change", (e) => {
                const value = $(e.target).val();
                if (value != "-1") {
                    $("#cmbCuentaDestino").prop("selectedIndex", 0);
                    $("#cmbCuentaDestino").children().each((index, el) => {
                        $(el).show();
                        if ($(el).val() == value) $(el).hide();
                    });
                }
            });

            $("#txtCBUDestino").on("focus", () => $("#opcCBU").prop("checked", true));
            $("#txtAliasDestino").on("focus", () => $("#opcAlias").prop("checked", true));
        });
    </script>

</body>

</html>