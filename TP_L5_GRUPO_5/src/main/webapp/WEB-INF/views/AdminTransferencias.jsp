<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
</head>

<body>
	<div class="tab-content" id="TabContent">
		<div class="tab-pane fade show active" id="listado" role="tabpanel" aria-labelledby="home-tab">
			<div class="container mt-3">
				<h1>Transferencias</h1>
				<hr>
				<div class="row mb-3">
					<div class="container-fluid">
						<div class="accordion" id="cont-filtros">
							<div class="card">
								<div class="card-header">
									<a class="btn btn-link btn-block text-left" data-toggle="collapse"
										href="#collapseFiltros" role="button" aria-expanded="false"> Filtros </a>
								</div>

								<div id="collapseFiltros" class="collapse" data-parent="#cont-filtros">
									<div class="card-body">
										<div class="row">
											<div class="col-sm-4">
												<select id="cmbFiltroEstado" class="form-control">
													<option>Filtrar por Estado</option>
													<c:forEach var="est" items="${listaEstados}">
														<option>${est.getDescripcion()}</option>
													</c:forEach>
												</select>
											</div>
											<div class="col-sm-2">
												<button id="btnReiniciarFiltros"
													class="btn btn-secondary">Reiniciar</button>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div>
					<table id="tbLTransferencias">
						<thead>
							<tr style="background-color: deepskyblue;">
								<th>Fecha</th>
								<th>CBU de origen</th>
								<th>CBU de destino</th>
								<th>Monto</th>
								<th>Estado</th>
							</tr>
						</thead>
						<c:forEach var="transferencia" items="${listaTransferencias}">
							<tr style="background-color: bisque;">
								<td>${transferencia.getFecha()}</td>
								<td>${transferencia.getCuentaOrigen().getCBU()}</td>
								<td>${transferencia.getCuentaDestino().getCBU()}</td>
								<td>${transferencia.getMonto()}</td>
								<td>${transferencia.getEstado().getDescripcion()}</td>
							</tr>
						</c:forEach>
					</table>
				</div>
			</div>
		</div>
	</div>

	<script>
		$(document).ready(function () {
			initTable("tbLTransferencias", {
				order: [[ 0, 'desc' ]],
				orderCellsTop: true,
				fixedHeader: true,
				columnDefs: [{
					targets: 0, render: data => {
						return moment(data).format('DD/MM/YYYY HH:mm');
					}
				}]
			});

			// Setup - add a text input to each footer cell
			$('#tbLTransferencias thead tr').clone(true).appendTo('#tbLTransferencias thead');
			$('#tbLTransferencias thead tr:eq(1) th').each(function (i) {
				var title = $(this).text();
				$(this).html('<input type="text" placeholder="Buscar ' + title + '"/>');
				let table = $("#tbLTransferencias").DataTable();

				$('input', this).on('keyup change', function () {
					if (table.column(i).search() !== this.value) {
						table
							.column(i)
							.search(this.value)
							.draw();
					}
				});
			});

			$("#collapseFiltros").on("change", "#cmbFiltroEstado", (e) => {
                var valor = $("option:selected", $(e.currentTarget)).val();
                FilterByColumn($("#tbLTransferencias").DataTable(), "Estado", valor);
			});
			
			$("#collapseFiltros").on("click", "#btnReiniciarFiltros", () => {
                ClearFields("cont-filtros");
                RemoveFilters($("#tbLTransferencias").DataTable());
            });
		});
	</script>
</body>

</html>