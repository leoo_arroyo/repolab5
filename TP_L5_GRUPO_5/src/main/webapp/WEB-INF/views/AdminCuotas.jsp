<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Cuotas</title>
</head>

<body>

    <div class="container mt-3">
        <div class="row">
            <div class="col-md-6">
                <h2>Cuotas</h2>
            </div>
            <div class="col-md-6 text-right">
                <a id="btnVolver" href="./prestamos">Volver a Prestamos</a>
            </div>
        </div>
        <hr />

        <div id="tblListado">
            <table id="tblCuotas">
                <thead>
                    <tr style="background-color: deepskyblue;">
                        <th>N° de cuota</th>
                        <th>Fecha de pago</th>
                        <th>Estado</th>
                    </tr>
                </thead>
                <c:forEach var="cuota" items="${listaCuotas}">
                    <tr style="background-color: bisque;">
                        <td>${cuota.getNumeroDeCuota()}</td>
                        <td>${cuota.getFechaDePago()}</td>
                        <c:choose>
                            <c:when test="${cuota.getEstadoDePago()==false}">
                                <td>Sin pagar</td>
                            </c:when>
                            <c:otherwise>
                                <td>Pagado</td>
                            </c:otherwise>
                        </c:choose>
                    </tr>
                </c:forEach>
            </table>
        </div>
    </div>

    <script>
        $(() => {
            initTable("tblCuotas", {
                lengthChange: false,
                paging: false,
                columnDefs: [{
                    targets: 1, render: data => {
                        return (data) ? moment(data).format('DD/MM/YYYY') : "Pendiente";
                    }
                }]
            });

            $("#btnVolver").on("click", (e) => {
                e.preventDefault();
                GetContentPage().load($(e.target).attr("href"));
            });
        });
    </script>

</body>

</html>