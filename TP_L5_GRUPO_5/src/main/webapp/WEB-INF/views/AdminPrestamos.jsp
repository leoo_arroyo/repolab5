<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
</head>

<body>

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item" role="presentation">
            <a class="nav-link active" id="listado-tab" data-toggle="tab" href="#listado" role="tab"
                aria-controls="home" aria-selected="true">Listado</a>
        </li>
        <li class="nav-item" role="presentation">
            <a class="nav-link" id="detalle-tab" data-toggle="tab" href="#detalle" role="tab" aria-controls="profile"
                aria-selected="false">Detalle del usuario</a>
        </li>
    </ul>

    <div class="tab-content" id="TabContent">
        <div class="tab-pane fade show active" id="listado" role="tabpanel" aria-labelledby="home-tab">
            <div class="container mt-3">
                <h1>Solicitudes de préstamos</h1>
                <hr>
                <div class="row mb-3">
                    <div class="container-fluid">
                        <div class="accordion" id="cont-filtros">
                            <div class="card">
                                <div class="card-header">
                                    <a class="btn btn-link btn-block text-left" data-toggle="collapse"
                                        href="#collapseFiltros" role="button" aria-expanded="false"> Filtros </a>
                                </div>

                                <div id="collapseFiltros" class="collapse" data-parent="#cont-filtros">
                                    <div class="card-body">
                                        <div class="row">
                                            <div class="col-sm-4">
                                                <select id="cmbFiltroEstado" class="form-control">
                                                    <option>Filtrar por Estado</option>
                                                    <option>Aceptado</option>
                                                    <option>Rechazado</option>
                                                    <option>Aceptar Rechazar</option>
                                                </select>
                                            </div>
                                            <div class="col-sm-2">
                                                <button id="btnReiniciarFiltros"
                                                    class="btn btn-secondary">Reiniciar</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div>
                    <table id="tblPrestamos">
                        <thead>
                            <tr style="background-color: deepskyblue;">
                                <th>Cliente</th>
                                <th>CBU de destino</th>
                                <th>Alias de la cuenta</th>
                                <th>Préstamo solicitado</th>
                                <th>Plazo</th>
                                <th>Importe por cuota</th>
                                <th>Fecha de solicitud</th>
                                <th>Fecha de aprobación</th>
                                <th>Estado</th>
                            </tr>
                        </thead>
                        <c:forEach var="prestamo" items="${listaPrestamos}">
                            <tr style="background-color: bisque;">
                                <td><a id="btnDetalle" class="btn btn-success btn-sm" title="Vista detallada"
                                        href="./usuarios/${prestamo.getUsuario().getUser()}">${prestamo.getUsuario().getUser()}</a>
                                </td>
                                <td>${prestamo.getCuenta().getCBU()}</td>
                                <td>${prestamo.getCuenta().getAlias()}</td>
                                <td>$ ${prestamo.getImporte()}</td>
                                <td>${prestamo.getPlazo()}</td>
                                <td>$ ${prestamo.getImportePorCuota()}</td>
                                <td>${prestamo.getFechaSolicitud()}</td>
                                <td>${prestamo.getFechaAprobacion()}</td>
                                <c:choose>
                                    <c:when test="${prestamo.getEstado().getId()==1}">
                                        <td class="btn-group">
                                            <a id="Aceptar" class="btn btn-success" title="Aceptar"
                                                href="./prestamos/aceptar/${prestamo.getIdPrestamo()}">Aceptar</a>
                                            <a id="Rechazar" class="btn btn-danger" title="Rechazar"
                                                href="./prestamos/rechazar/${prestamo.getIdPrestamo()}">Rechazar</a>
                                        </td>
                                    </c:when>
                                    <c:when test="${prestamo.getEstado().getId()==2}">
                                        <td>Aceptado
                                            <a id="btnCuotas" class="btn btn-info btn-sm" title="Vista cuotas"
                                                href="./prestamos/cuotas?idPrestamo=${prestamo.getIdPrestamo()}">
                                                <i class="far fa-file"></i>
                                            </a></td>
                                    </c:when>
                                    <c:when test="${prestamo.getEstado().getId()==3}">
                                        <td>Rechazado</td>
                                    </c:when>
                                </c:choose>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="detalle" role="tabpanel" aria-labelledby="profile-tab">
            <div id="lblSinResultados" class="row justify-content-center mt-3">
                <h3 class="font-weight-bold">Sin resultados</h3>
            </div>
            <div id="panelDatos" class="container mt-3" style="display: none;">
                <h2>Detalle del usuario</h2>
                <hr />

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Usuario</span>
                        </div>
                        <input type="text" id="detUser" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Email</span>
                        </div>
                        <input type="text" id="detEmail" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-2 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">DNI</span>
                        </div>
                        <input type="text" id="detDNI" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-10 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Apellido y nombre</span>
                        </div>
                        <input type="text" id="detNombre" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Fecha de nacimiento</span>
                        </div>
                        <input type="text" id="detFechaNac" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Nacionalidad</span>
                        </div>
                        <input type="text" id="detNacionalidad" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Provincia</span>
                        </div>
                        <input type="text" id="detProvincia" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Localidad</span>
                        </div>
                        <input type="text" id="detLocalidad" class="form-control" disabled />
                    </div>
                </div>

                <div class="row">
                    <div class="input-group col-sm-6 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Dirección</span>
                        </div>
                        <input type="text" id="detDireccion" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-3 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Sexo</span>
                        </div>
                        <input type="text" id="detSexo" class="form-control" disabled />
                    </div>
                    <div class="input-group col-sm-3 p-1">
                        <div class="input-group-prepend">
                            <span class="input-group-text font-weight-bold">Estado</span>
                        </div>
                        <input type="text" id="detEstado" class="form-control" disabled />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(document).ready(function () {
            initTable("tblPrestamos", {
                order: [[ 6, 'desc' ]],
                orderCellsTop: true,
                fixedHeader: true,
                columnDefs: [{
                    targets: [6, 7], render: data => (data) ? moment(data).format('DD/MM/YYYY') : "-"
                }]
            });
            // Setup - add a text input to each footer cell
            $('#tblPrestamos thead tr').clone(true).appendTo('#tblPrestamos thead');
            $('#tblPrestamos thead tr:eq(1) th').each(function (i) {
                var title = $(this).text();
                $(this).html('<input type="text" placeholder="Buscar ' + title + '"/>');
                let table = $("#tblPrestamos").DataTable();

                $('input', this).on('keyup change', function () {
                    if (table.column(i).search() !== this.value) {
                        table
                            .column(i)
                            .search(this.value)
                            .draw();
                    }
                });
            });

            $("#Aceptar").on("click", () => {
                event.preventDefault();
                var url = $(event.currentTarget).attr("href");

                swal({
                    title: "¿Está seguro que desea aceptar el prestamo?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((borrar) => {
                    if (borrar) {
                        $.get({
                            url: url,
                            success: (res) => {
                                if (res == "OK")
                                    swal("Éxito!").then(() => location.reload(true));
                            }
                        });
                    }
                });
            });

            $("#Rechazar").on("click", () => {
                event.preventDefault();
                var url = $(event.currentTarget).attr("href");
                swal({
                    title: "¿Está seguro que desea rechazar el préstamo?",
                    icon: "warning",
                    buttons: true,
                    dangerMode: true,
                }).then((borrar) => {
                    if (borrar) {
                        $.get({
                            url: url,
                            success: (res) => {
                                if (res == "OK")
                                    swal("Éxito!").then(() => location.reload(true));
                            }
                        });
                    }
                });
            });


            const EventoPrestamo = (idElement, text) => {
                $("#TabContent").on("click", `[id$=${idElement}]`, (event) => {
                    event.preventDefault();
                    var url = $(event.currentTarget).attr("href");
                    swal({
                        title: text,
                        icon: "warning",
                        buttons: true,
                        dangerMode: true,
                    }).then((borrar) => {
                        if (borrar) {
                            $.get({
                                url: url,
                                success: (res) => {
                                    if (res == "OK")
                                        swal("Éxito!").then(() => location.reload(true));
                                }
                            });
                        }
                    });
                });
            }

            $("#collapseFiltros").on("change", "#cmbFiltroEstado", (e) => {
                var valor = $("option:selected", $(e.currentTarget)).val();
                FilterByColumn($("#tblPrestamos").DataTable(), "Estado", valor);
            });

            $("#collapseFiltros").on("click", "#btnReiniciarFiltros", () => {
                ClearFields("cont-filtros");
                RemoveFilters($("#tblPrestamos").DataTable());
            });

            $("#TabContent").on("click", "#btnDetalle", function (event) {
                event.preventDefault();
                $("#collapseCuentas").removeClass("show");
                $("#rowCuentas").empty();

                $.get({
                    url: $(this).attr("href"),
                    success: function (res) {
                        if (res.message == "OK") {
                            $('#detalle-tab').tab('show');
                            $("#lblSinResultados").hide();
                            $("#panelDatos").show();

                            var usuario = res.data;
                            $("[id$=detUser]").val(usuario.user);
                            $("[id$=detDNI]").val(usuario.dni);
                            $("[id$=detEmail]").val(usuario.email);
                            $("[id$=detNombre]").val(usuario.apellido + " " + usuario.nombre);

                            var fechaNac = usuario.fechaDeNacimiento;
                            $("[id$=detFechaNac]").val(`\${fechaNac.dayOfMonth}/\${fechaNac.monthValue}/\${fechaNac.year}`);
                            $("[id$=detNacionalidad]").val(usuario.nacionalidad.nombre);
                            $("[id$=detProvincia]").val(usuario.localidad.provincia.nombre);
                            $("[id$=detLocalidad]").val(usuario.localidad.nombre);
                            $("[id$=detDireccion]").val(usuario.direccion);

                            var sexo = (usuario.sexo == "M") ? "Masculino" : "Femenino";
                            $("[id$=detSexo]").val(sexo);
                            var estado = (usuario.estado) ? "Activo" : "Suspendido";
                            $("[id$=detEstado]").val(estado);
                        }
                        else {
                            ShowWarning(res.message);
                        }
                    }
                });
            });

            $("#tblPrestamos").on("click", "#btnCuotas", function (event) {
                event.preventDefault();
                var ruta = $(this).attr("href");
                GetContentPage().load(ruta);
            });

        });
    </script>
</body>

</html>