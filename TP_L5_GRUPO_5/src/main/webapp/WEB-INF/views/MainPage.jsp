<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<!DOCTYPE html>
<html>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Admin Home</title>

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css"
		integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">

	<!-- Fontawesome -->
	<link rel="stylesheet" href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css"
		integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" crossorigin="anonymous" />

	<!-- DataTable CSS -->
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css ">

	<!-- SideBar -->
	<link rel="stylesheet" href=".//resources/assets/CSS/sidebar.css">

	<!-- Main -->
	<link rel="stylesheet" href=".//resources/assets/CSS/Main.css">
</head>

<body class="bg-light">

	<div class="wrapper">
		<!-- Sidebar Holder -->
		<nav id="sidebar">
			<div class="sidebar-header">
				<h2><i class="fas fa-coins"></i> <i>MegaBank</i></h2>
			</div>

			<ul class="list-unstyled components">
				<c:forEach var="menu" items="${listaMenus}">
					<li class="active">
						<a href="#${menu.getNombre()}" data-toggle="collapse" aria-expanded="false"
							class="dropdown-toggle">${menu.getNombre()}</a>
						<ul class="collapse list-unstyled" id="${menu.getNombre()}">
							<c:forEach var="pagina" items="${menu.getPaginas()}">
								<li>
									<a id="itemPagina" href="./${pagina.getUrl()}">${pagina.getNombre()}</a>
								</li>
							</c:forEach>
						</ul>
					</li>
				</c:forEach>
			</ul>
		</nav>

		<!-- Page Content Holder -->
		<div id="content">

			<nav class="navbar navbar-expand-lg navbar-light bg-light">
				<div class="container-fluid">

					<button type="button" id="sidebarCollapse" class="navbar-btn">
						<span></span>
						<span></span>
						<span></span>
					</button>
					<button class="btn btn-dark d-inline-block d-lg-none ml-auto" type="button" data-toggle="collapse"
						data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
						aria-expanded="false" aria-label="Toggle navigation">
						<i class="fas fa-align-justify"></i>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="nav navbar-nav ml-auto">
							<li class="nav-item active">
								<div class="dropdown float-right">
									<a class="btn btn-secondary dropdown-toggle" href="#" role="button"
										id="dropdownUsuario" data-toggle="dropdown" aria-haspopup="true"
										aria-expanded="false">${user}</a>
									<div class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownUsuario">
										<a class="dropdown-item" href="./logout">Salir</a>
									</div>
								</div>
							</li>
						</ul>
					</div>
				</div>
			</nav>

			<div id="content-page"></div>
		</div>
	</div>

	<input type="hidden" id="lastPage" value="${lastPage}">

	<!-- jQuery -->
	<script src="https://code.jquery.com/jquery-3.5.1.js"></script>

	<!-- Popper.js -->
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"></script>

	<!-- Bootstrap JS -->
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>

	<!-- DataTable JS -->
	<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
	<script src="https://cdn.datatables.net/fixedheader/3.1.7/js/dataTables.fixedHeader.min.js"></script>

	<!-- SweetAlert -->
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<!-- jQuery Mask -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.mask/1.14.10/jquery.mask.js"></script>

	<!-- Font Awesome JS -->
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
		integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ"
		crossorigin="anonymous"></script>
	<script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"
		integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY"
		crossorigin="anonymous"></script>

	<!-- Moment JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>

	<!-- Chart JS -->
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.js"
		integrity="sha256-nZaxPHA2uAaquixjSDX19TmIlbRNCOrf5HO1oHl5p70=" crossorigin="anonymous"></script>

	<script src=".//resources/assets/JS/Funciones.js"></script>
	<script src=".//resources/assets/JS/Tablas.js"></script>

	<script>
		$(() => {
			$('#sidebarCollapse').on('click', function () {
				$('#sidebar').toggleClass('active');
				$(this).toggleClass('active');
			});

			$("[id=itemPagina]").on("click", function (event) {
				event.preventDefault();
				GetContentPage().load($(this).attr("href"));
			});

			var ultimaPagina = $("#lastPage").val()
			if (ultimaPagina != "") {
				GetContentPage().load("." + ultimaPagina);
			}
		});
	</script>
</body>

</html>