<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<meta charset="UTF-8">
</head>

<body>
    <div class="container mt-3">
        <h1>Reportes</h1>
        <hr>
        <div class="row mb-3">
            <div class="col-md-12">
                <div class="card card-primary">
                    <div class="card-header">
                        <h3 class="card-title">Resumen anual de préstamos</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart">
                            <canvas id="areaCantidadPrestada"
                                style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                        </div>
                    </div>
                </div>

                <div class="card card-danger">
                    <div class="card-header">
                        <h3 class="card-title">Tipos de cuentas</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        Desde: <input type="date" id="txtRepCuentasInicio"> hasta: <input type="date"
                            id="txtRepCuentasFin">
                        <button id="btnRepCuentasBuscar" class="btn btn-secondary">Buscar</button>
                        <canvas id="donutChart"
                            style="min-height: 250px; height: 250px; max-height: 250px; max-width: 100%;"></canvas>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <script>
        var donutChart = {};
        
        async function initChartPrestamos() {
            let result = await fetch("./reportes/prestamos").then(response => {
                if (response.ok) {
                    return response.json();
                }
                else
                    throw new Error(response.statusText);
            }).catch(console.log);

            var areaChartCanvas = $('#areaCantidadPrestada').get(0).getContext('2d')

            var areaChartData = {
                labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
                datasets: [
                    {
                        label: 'Digital Goods',
                        backgroundColor: 'rgba(60,141,188,0.9)',
                        borderColor: 'rgba(60,141,188,0.8)',
                        pointRadius: false,
                        pointColor: '#3b8bba',
                        pointStrokeColor: 'rgba(60,141,188,1)',
                        pointHighlightFill: '#fff',
                        pointHighlightStroke: 'rgba(60,141,188,1)',
                        data: result.data
                    },
                ]
            }

            var areaChartOptions = {
                maintainAspectRatio: false,
                responsive: true,
                legend: {
                    display: false
                },
                scales: {
                    xAxes: [{
                        gridLines: {
                            display: false,
                        }
                    }],
                    yAxes: [{
                        gridLines: {
                            display: false,
                        }
                    }]
                }
            }

            var areaChart = new Chart(areaChartCanvas, {
                type: 'line',
                data: areaChartData,
                options: areaChartOptions
            });
        }

        async function initChartCuentas() {
            let result = await fetch("./reportes/cuentas").then(response => {
                if (response.ok) {
                    return response.json();
                }
                else
                    throw new Error(response.statusText);
            }).catch(console.log);

            var donutChartCanvas = $('#donutChart').get(0).getContext('2d')
            var donutData = {
                labels: [
                    'Caja de ahorro en pesos',
                    'Caja de ahorro en dólares',
                    'Cuenta corriente',
                    'Cuenta corriente especial en pesos',
                    'Cuenta corriente especial en dólares',
                ],
                datasets: [
                    {
                        data: result.data,
                        backgroundColor: ['#f56954', '#00a65a', '#f39c12', '#00c0ef', '#3c8dbc'],
                    }
                ]
            }
            var donutOptions = {
                maintainAspectRatio: false,
                responsive: true,
            }
            donutChart = new Chart(donutChartCanvas, {
                type: 'pie',
                data: donutData,
                options: donutOptions
            });
        }

        $(() => {
            initChartPrestamos();
            initChartCuentas();

            $("#btnRepCuentasBuscar").on("click", async () => {
                let fInicio = $("#txtRepCuentasInicio").val();
                let fFin = $("#txtRepCuentasFin").val();
                let result = await fetch(`./reportes/cuentas?fechaInicio=\${fInicio}&fechaFin=\${fFin}`).then(response => {
                    if (response.ok) {
                        return response.json();
                    }
                    else
                        throw new Error(response.statusText);
                }).catch(console.log);

                donutChart.data.datasets[0].data = result.data;
                donutChart.update();
            });
        });
    </script>
</body>

</html>