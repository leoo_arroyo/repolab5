<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix = "fmt" uri = "http://java.sun.com/jsp/jstl/fmt" %>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Cuotas</title>
</head>

<body>

    <div class="container mt-3">
        <div class="row">
            <div class="col-md-6">
                <h2>Cuotas</h2>
            </div>
            <div class="col-md-6 text-right">
                <a id="btnVolver" href="./prestamos">Volver a Prestamos</a>
            </div>
        </div>
        <hr />

        <div class="row mb-3">
            <div class="container-fluid">
                <div class="card">
                    <div class="card-header">
                        <h5 class="card-title">Datos del préstamo</h5>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-6">
                                <fmt:parseDate value="${prestamo.getFechaSolicitud()}" var="fecha" pattern="yyyy-MM-dd" />
                                <p class="card-text">Solicitado el
                                    <fmt:formatDate pattern="dd-MM-yyyy" value="${fecha}" />
                                </p>
                            </div>
                            <div class="col-6 text-right">
                                <p class="card-text">Importe: $${prestamo.getImporte()}</p>
                            </div>
                        </div>
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            <div class="form-group col-12">
                                <label>Selección de cuenta</label>
                                <select id="cmbCuenta" class="form-control">
                                    <c:forEach var="c" items="${listaCuentas}">
                                        <option value="${c.getCBU()}">${c.getAlias()} - $${c.getSaldo()}</option>
                                    </c:forEach>
                                </select>
                                <div><label class="text-muted">Elegi una cuenta para pagar las cuotas. Puede ser
                                        cualquiera de tus cuentas que tenga el saldo suficiente.</label></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="tblListado">
            <div id="divHeader">
                <div class="row text-center p-3">
                    <div class="col-3 font-weight-bold">Nº</div>
                    <div class="col-3 font-weight-bold">Fecha de pago</div>
                    <div class="col-3 font-weight-bold">Importe</div>
                </div>
            </div>

            <c:forEach var="c" items="${listaCuotas}">
                <div id="divCuota" class="rounded">
                    <div class="row text-center p-3">
                        <div class="col-3">${c.getNumeroDeCuota()}</div>
                        <c:choose>
                            <c:when test="${c.getFechaDePago()!=null}">
                                <fmt:parseDate value="${c.getFechaDePago()}" var="fecha" pattern="yyyy-MM-dd" />
                                <div class="col-3"><fmt:formatDate pattern="dd-MM-yyyy" value="${fecha}" /></div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-3">Pendiente</div>
                            </c:otherwise>
                        </c:choose>
                        <div class="col-3">$${prestamo.getImportePorCuota()}</div>
                        <c:choose>
                            <c:when test="${c.getEstadoDePago()==true}">
                                <div class="col-3">
                                    <span class="badge badge-success">PAGADO</span>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="col-3">
                                    <a id="btnPagar" class="btn btn-primary" href="#" data-id="${c.getIdCuota()}">Pagar</a>
                                </div>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </c:forEach>
        </div>
    </div>

    <input type="hidden" id="hdfIdPrestamo" value="${prestamo.getIdPrestamo()}" />

    <script>
        $(() => {
            $("*[id=divCuota]").each((index, el) => {
                let color = (index % 2 == 0) ? "#8eaac3" : "#ade0e8";
                $(el).css("background-color", color);
            });

            $("#btnVolver").on("click", (e) => {
                e.preventDefault();
                GetContentPage().load($(e.target).attr("href"));
            });

            $("#tblListado").on("click", "#btnPagar", (e) => {
                e.preventDefault();
                let data = {
                    idCuota: $(e.target).attr("data-id"),
                    idCuenta: $("#cmbCuenta").val()
                }

                $.post({
                    url: "./prestamos/pagar",
                    data: data,
                    success: res => {
                        if (res.message == "OK") {
                            let idPrestamo = $("#hdfIdPrestamo").val();
                            swal("Éxito!").then(() => GetContentPage().load(`./prestamos/cuotas?idPrestamo=\${idPrestamo}`));
                        }
                        else {
                            ShowWarning(res.details);
                        }
                    }
                })
            })
        });
    </script>

</body>

</html>