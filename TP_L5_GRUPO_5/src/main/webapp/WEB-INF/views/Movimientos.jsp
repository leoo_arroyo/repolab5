<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <title>Movimientos</title>
</head>

<body>

    <div class="container mt-3">
        <c:choose>
            <c:when test="${message != null}">
                <h2>Aún no tenés cuentas disponibles.</h2>
            </c:when>
            <c:otherwise>
                <h2>Movimientos</h2>
                <hr />

                <div class="row mb-3">
                    <div class="container-fluid">
                        <div class="card">
                            <div class="card-header">
                                <div class="row">
                                    <div class="col-md-6">
                                        <h5 class="card-title">Nº: ${cuenta.getNumeroDeCuenta()}</h5>
                                    </div>
                                    <div class="col-md-6 text-right">
                                        <h5 class="card-title">${cuenta.getAlias()}</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-6">
                                        <p class="card-text">${cuenta.getTipoDeCuenta().getDescripcion()}</p>
                                    </div>
                                    <div class="col-6 text-right">
                                        <p class="card-text">$${cuenta.getSaldo()}</p>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${cuenta.getEsPrincipal()==true}">
                                        <p class="card-text"><small class="text-muted">*Cuenta principal</small></p>
                                    </c:when>
                                </c:choose>
                            </div>
                        </div>
                    </div>
                </div>

                <div id="tblListado">
                    <table id="tblMovimientos">
                        <thead>
                            <tr style="background-color: deepskyblue;">
                                <th>Fecha</th>
                                <th>Detalle</th>
                                <th>Importe</th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="mov" items="${listaMovimientos}">
                                <tr style="background-color: bisque;">
                                    <td>${mov.getFecha()}</td>
                                    <td>${mov.getDetalle()}</td>
                                    <c:choose>
                                        <c:when test="${mov.getSigno().toString()=='+'}">
                                            <td>$${mov.getImporte()}</td>
                                        </c:when>
                                        <c:otherwise>
                                            <td>- $${mov.getImporte()}</td>
                                        </c:otherwise>
                                    </c:choose>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
            </c:otherwise>
        </c:choose>
    </div>

    <script>
        $(() => {
            initTable("tblMovimientos", {
                order: [[ 0, 'desc' ]],
                columnDefs: [{
                    targets: 0, render: data => {
                        return moment(data).format('DD/MM/YYYY HH:mm');
                    }
                }]
            });
        });
    </script>

</body>

</html>